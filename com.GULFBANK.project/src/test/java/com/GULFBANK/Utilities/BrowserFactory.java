package com.GULFBANK.Utilities;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;

public class BrowserFactory {

	public static Readconfig readconfig = new Readconfig();
	public static WebDriver driver;

	public static WebDriver getBrowser(String BrowserName) {
		if (BrowserName.equalsIgnoreCase("Firefox")) {
			System.setProperty("webdriver.firefox.driver", readconfig.GetFirefoxpath());
			driver = new FirefoxDriver();
		} else if (BrowserName.equalsIgnoreCase("Chrome")) {
			System.setProperty("webdriver.chrome.driver", readconfig.GetChromepath());
			driver = new ChromeDriver();
		} else if (BrowserName.equalsIgnoreCase("Edge")) {
			System.setProperty("webdriver.edge.driver", readconfig.GetEdgepath());
			driver = new EdgeDriver();
		} else if (BrowserName.equalsIgnoreCase("Safari")) {
			System.setProperty("webdriver.safari.driver", readconfig.GetSafaripath());
			driver = new SafariDriver();
		}

		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		return driver;
	}

	public static void quitBrowser(WebDriver driver) throws IOException {
		driver.quit();
		Runtime rt = Runtime.getRuntime();
		Process proc = rt.exec("taskkill /im chrome.exe /f /t");
		System.out.println("TaskManager chrome process killed" + proc);
		Runtime.getRuntime().exec("taskkill /F /IM chromedriver.exe /T");
		Runtime.getRuntime().exec("taskkill /F /IM geckodriver.exe /T");
	}
}