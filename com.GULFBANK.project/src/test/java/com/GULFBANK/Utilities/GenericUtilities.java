package com.GULFBANK.Utilities;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.GULFBANK.Base.ExtentTestManager;
import com.GULFBANK.Base.TestBase;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;

public class GenericUtilities extends TestBase {

	public static String generateStringFromResource(String path) throws IOException {
		return new String(Files.readAllBytes(Paths.get(path)));
	}

	static WebDriverWait wait = new WebDriverWait(driver, 15);
	static Actions action = new Actions(driver);

	public static void click(WebElement ele) {
		wait.until(ExpectedConditions.visibilityOf(ele));
		WebElement element = wait.until(ExpectedConditions.elementToBeClickable(ele));
		element.click();
	}

	public static WebElement waitTillClickable(WebElement ele) {
		wait.until(ExpectedConditions.visibilityOf(ele));
		return wait.until(ExpectedConditions.elementToBeClickable(ele));
	}

	public static WebElement waitForElementUsingFluedWait(String xpath) {
		FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver);
		wait.withTimeout(10, TimeUnit.SECONDS);
		wait.pollingEvery(250, TimeUnit.MILLISECONDS);
		wait.ignoring(NoSuchElementException.class);
		WebElement element = wait.until(new Function<WebDriver, WebElement>() {
			public WebElement apply(WebDriver driver) {
				return driver.findElement(By.xpath(xpath));
			}
		});
		return element;
	}

	public static WebElement waitTillVisibility(WebElement ele) {
		wait.until(ExpectedConditions.visibilityOf(ele));
		return wait.until(ExpectedConditions.visibilityOf(ele));
	}

	public static void entertext(WebElement ele, String txt) {
		WebElement element = wait.until(ExpectedConditions.visibilityOf(ele));
		element.clear();
		element.sendKeys(txt);
	}

	public static void waitForTextToAppear(String textToAppear, By ele) {
		wait.until(ExpectedConditions.textToBePresentInElementLocated(ele, textToAppear));
	}

	public static void uploadFile(String filePath) throws AWTException {
		Robot robot = new Robot();
		robot.setAutoDelay(2000);
		StringSelection stringselection = new StringSelection(System.getProperty("user.dir") + filePath);

		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringselection, null);
		robot.setAutoDelay(2000);
		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_CONTROL);
		robot.keyRelease(KeyEvent.VK_V);
		robot.setAutoDelay(2000);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
	}

	public static void scrolluntilElementVisible(By element) {
		WebElement element_visible = wait.until(ExpectedConditions.visibilityOfElementLocated(element));
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView(true);", element_visible);
	}

	public static void entertext(By ele, String txt) {
		WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(ele));
		element.clear();
		element.sendKeys(txt);
	}

	public static String getText(By element) {
		WebElement text = wait.until(ExpectedConditions.visibilityOfElementLocated(element));
		String actual_text = text.getText();
		return actual_text;
	}

	public static String getTimeStamp() {
		String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
		return timeStamp;
	}

	public static void printExtentPassLog(String expected, String actual) throws Throwable {
		ExtentTestManager.getTest()
				.log(Status.PASS, "Expected ------> " + expected + "     |  Actual ------> " + actual,
						MediaEntityBuilder
								.createScreenCaptureFromPath(
										GenericUtilities.captureScreen(driver, GenericUtilities.getTimeStamp()))
								.build());
	}

	public static void printExtentFailLog(String expected, Exception e) throws Throwable {
		ExtentTestManager.getTest()
				.log(Status.FAIL,
						"Expected ------> " + expected + "    | Actual ------> " + e.getMessage().substring(0, 175)
								+ expected,
						MediaEntityBuilder
								.createScreenCaptureFromPath(
										GenericUtilities.captureScreen(driver, GenericUtilities.getTimeStamp()))
								.build());
	}

	public static void printExtentConditionLog(Boolean condition, String expectedMessage, String actualMessage)
			throws Throwable {
		if (condition) {
			ExtentTestManager.getTest()
					.log(Status.PASS,
							"Condition passed ------> " + condition.toString() + "    | Expected ------> "
									+ expectedMessage + "   | Actual ------> " + actualMessage,
							MediaEntityBuilder
									.createScreenCaptureFromPath(
											GenericUtilities.captureScreen(driver, GenericUtilities.getTimeStamp()))
									.build());
		} else {
			ExtentTestManager.getTest()
					.log(Status.FAIL,
							"Condition failed ------> " + condition.toString() + "    | Expected  ------> "
									+ expectedMessage + "   | Actual ------> " + actualMessage,
							MediaEntityBuilder
									.createScreenCaptureFromPath(
											GenericUtilities.captureScreen(driver, GenericUtilities.getTimeStamp()))
									.build());

		}
	}

	public static void HighlightElement(WebDriver driver, WebElement elm) throws IOException {
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].style.border='4px groove red'", elm);
			ExtentTestManager.getTest()
					.log(Status.INFO, "**** Highilighted ------> ",
							MediaEntityBuilder
									.createScreenCaptureFromPath(
											GenericUtilities.captureScreen(driver, GenericUtilities.getTimeStamp()))
									.build());
			Thread.sleep(1000);
			js.executeScript("arguments[0].style.border=''", elm);
		} catch (Exception e) {
			ExtentTestManager.getTest()
					.log(Status.FAIL, "**** Highlight failed ------> ",
							MediaEntityBuilder
									.createScreenCaptureFromPath(
											GenericUtilities.captureScreen(driver, GenericUtilities.getTimeStamp()))
									.build());
		}
	}

	public static void check_box_Y(By element) {
		WebElement check_box_Y = wait.until(ExpectedConditions.visibilityOfElementLocated(element));

		if (!check_box_Y.isSelected()) {
			GenericUtilities.sleep(3);
			check_box_Y.click();
		}
	}

	public static void elementVisible(By ele) {
		wait.until(ExpectedConditions.visibilityOfElementLocated(ele));
	}

	public static void elementVisible(WebElement ele) {
		wait.until(ExpectedConditions.visibilityOf(ele));
	}

	public static void staleElement(WebElement ele) {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.stalenessOf(ele)));
	}

	public static void elementNotVisible(By ele) {
		wait.until(ExpectedConditions.invisibilityOfElementLocated(ele));
	}

	public static void doubleClick(By element) {
		WebElement acc = wait.until(ExpectedConditions.visibilityOfElementLocated(element));
		action.doubleClick(acc).build().perform();
	}

	public static void check_box_N(By element) {
		WebElement check_box_N = wait.until(ExpectedConditions.visibilityOfElementLocated(element));

		if (check_box_N.isSelected()) {
			GenericUtilities.sleep(3);
			check_box_N.click();
		}
	}

	public static void Retry_stale_click(WebElement element) {
		boolean staleElement = true;
		while (staleElement) {
			try {

				GenericUtilities.click(element);
				staleElement = false;
				// logger.info("try-----");
			} catch (StaleElementReferenceException e) {
				// logger.info("catch -----");
				staleElement = true;
			}
		}
	}

	public static void Retry_stale_click(WebElement element, String txt) {
		boolean staleElement = true;
		while (staleElement) {
			try {
				GenericUtilities.entertext(element, txt);
				staleElement = false;
			} catch (StaleElementReferenceException e) {
				staleElement = true;
			}
		}
	}

	public static void click(By ele) {
		WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(ele));
		element.click();
	}

	public static void pressEnter() {
		Robot robot = null;
		try {
			robot = new Robot();
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
		} catch (AWTException e) {
			e.printStackTrace();
		}
	}

	public static void pressTab() {
		Robot robot;
		try {
			robot = new Robot();
			robot.keyPress(KeyEvent.VK_TAB);
			robot.keyRelease(KeyEvent.VK_TAB);
		} catch (AWTException e) {
			e.printStackTrace();
		}
	}

	public static void scrollUP() {
		Robot robot;
		try {
			robot = new Robot();
			robot.keyPress(KeyEvent.VK_PAGE_UP);
			robot.keyRelease(KeyEvent.VK_PAGE_UP);
			GenericUtilities.sleep(3);

			robot.keyPress(KeyEvent.VK_PAGE_UP);
			robot.keyRelease(KeyEvent.VK_PAGE_UP);
			GenericUtilities.sleep(3);
			robot.keyPress(KeyEvent.VK_PAGE_UP);
			robot.keyRelease(KeyEvent.VK_PAGE_UP);
			GenericUtilities.sleep(3);
			robot.keyPress(KeyEvent.VK_PAGE_UP);
			robot.keyRelease(KeyEvent.VK_PAGE_UP);
			GenericUtilities.sleep(3);
		} catch (AWTException e) {
			e.printStackTrace();
		}
	}

	public static void waitForTextToAppear(String textToAppear, WebElement element) {
		wait.until(ExpectedConditions.textToBePresentInElement(element, textToAppear));
	}

	public static void selectByText(By ele, String text) {
		WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(ele));
		Select drop = new Select(element);
		drop.selectByVisibleText(text);
	}

	public static void selectByText(WebElement ele, String text) {
		WebElement element = wait.until(ExpectedConditions.visibilityOf(ele));
		Select drop = new Select(element);
		drop.selectByVisibleText(text);
	}

	public static void elementNotVisible(WebElement ele) {
		wait.until(ExpectedConditions.invisibilityOf(ele));
	}

	public static void sleep(int seconds) {
		try {
			Thread.sleep(seconds * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public static void jSentertxt(WebElement element, String text) {
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("arguments[0].value='text';", element);
	}

	public static void jSclick(WebElement element) {
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", element);
	}

	public static void scrolluntilElementVisible(WebElement element) {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView(true);", element);
	}

	public static boolean isElementPresent(By by, WebDriver driver) {
		boolean present;
		try {
			driver.findElement(by);
			present = true;
		} catch (NoSuchElementException e) {
			present = false;
		}
		return present;
	}

	public static void killprocess() throws Exception {
		Runtime rt = Runtime.getRuntime();
		Process proc = rt.exec("taskkill /im chrome.exe /f /t");
		driver.quit();
	}

	public static void arrowUp() {
		Robot robot;
		try {
			robot = new Robot();
			robot.keyPress(KeyEvent.VK_UP);
			robot.keyRelease(KeyEvent.VK_UP);
		} catch (AWTException e) {
			e.printStackTrace();
		}
	}

	public static void arrowDown() {
		Robot robot;
		try {
			robot = new Robot();
			robot.keyPress(KeyEvent.VK_DOWN);
			robot.keyRelease(KeyEvent.VK_DOWN);
		} catch (AWTException e) {
			e.printStackTrace();
		}
	}

	public static void arrowDown_action() {
		Actions action = new Actions(driver);
		action.sendKeys(Keys.ARROW_DOWN).build().perform();
	}

	public boolean VerifyisDisplayed(WebElement element) {
		if (element.isDisplayed()) {
			return true;
		} else
			return false;
	}

	public void alertAccept() {
		Alert alert = driver.switchTo().alert(); 
		GenericUtilities.sleep(4);
		alert.accept(); 
	}

	public String alertgetText() {
		Alert alert = driver.switchTo().alert(); 
		GenericUtilities.sleep(4);
		String messageOnAlert = alert.getText(); 
		return messageOnAlert;
	}

	public static void mouseOver(WebElement element) {
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.visibilityOf(element));
		action.moveToElement(element).build().perform();
	}

	public void dragandDrop(WebElement source, WebElement target) {
		wait.until(ExpectedConditions.visibilityOf(source));
		wait.until(ExpectedConditions.visibilityOf(target));
		action.dragAndDrop(source, target).build().perform();
	}

	public static void doubleClick(WebElement element) {
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.visibilityOf(element));
		action.doubleClick(element).build().perform();
	}

	public void rightClick(WebElement element) {
		wait.until(ExpectedConditions.visibilityOf(element));
		action.contextClick(element).build().perform();
	}

	public void enterTextUsingAction(WebElement element) {
		wait.until(ExpectedConditions.visibilityOf(element));
		action.sendKeys(element).build().perform();
	}

	public void clickUsingAction(WebElement element) {
		wait.until(ExpectedConditions.visibilityOf(element));
		action.click(element).build().perform();
	}

	public void enterUsingAction() {
		action.sendKeys(Keys.ENTER).build().perform();
	}

	public void enterUsingAction(WebElement element) {
		wait.until(ExpectedConditions.visibilityOf(element));
		action.sendKeys(element, Keys.ENTER).build().perform();
	}

	public void tabUsingAction() {
		action.sendKeys(Keys.TAB).build().perform();
	}

	public void handlemultipleKeyboard() {
		action.keyDown(Keys.CONTROL).sendKeys("a").keyUp(Keys.CONTROL).perform();
	}

	public static void Close() {
		GenericUtilities.sleep(5);
		driver.findElement(By.xpath("//img[contains(@src,'close.png')]")).click();
		GenericUtilities.sleep(5);
	}

	public static void entertxxt(By ele, String txt) {
		WebDriverWait wait = new WebDriverWait(driver, 120);
		WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(ele));
		element.clear();
		element.sendKeys(txt);
	}

	public static String getEleText(By element) {
		sleep(4);
		WebElement elem = driver.findElement(element);
		scrolluntilByElementVisible(elem);
		sleep(2);
		return getTextVal(element);
	}

	public static void scrolluntilByElementVisible(WebElement elem) {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView(true);", elem);
	}

	public static String getTextVal(By element) {
		WebDriverWait wait = new WebDriverWait(driver, 60);
		return wait.until(ExpectedConditions.visibilityOfElementLocated(element)).getText();
	}

	public static String numberGen() {
		long timeSeed = System.nanoTime();

		double randSeed = Math.random() * 1000;

		long midSeed = (long) (timeSeed * randSeed);

		String s = midSeed + "";
		String finalNumber = s.substring(0, 9);

		return finalNumber;
	}

	public static String randomNumber() {
		Random rand = new Random();
		String num = String.format("%04d", rand.nextInt(10000));
		return num;
	}

	public static void waitForLoad(WebDriver driver) {
		ExpectedCondition<Boolean> pageLoadCondition = new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver driver) {
				return ((JavascriptExecutor) driver).executeScript("return document.readyState").equals("complete");
			}
		};
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(pageLoadCondition);
	}
}