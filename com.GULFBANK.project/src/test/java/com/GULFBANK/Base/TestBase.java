package com.GULFBANK.Base;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;

import com.GULFBANK.Utilities.BrowserFactory;
import com.GULFBANK.Utilities.Readconfig;

public class TestBase {

	public Readconfig readconfig = new Readconfig();
	public String baseURL = readconfig.getApplicationURL();
	public String username = readconfig.getUserName();
	public String password = readconfig.getPassword();
	public static WebDriver driver;
	public static Logger logger;

	@Parameters("browser")
	@BeforeClass(alwaysRun = true)
	public void setup(String browser) {
		logger = Logger.getLogger("devpinoyLogger");

		PropertyConfigurator.configure("./Configurations/log4j.properties");

		if (browser.equals("chrome")) {
			driver = BrowserFactory.getBrowser(browser);
			ChromeOptions options = new ChromeOptions();
			// driver= new RemoteWebDriver(new URL(hubUrl), options);
			options.addArguments("ignore-certificate-errors");
			options.addArguments("enable-automation");
			options.addArguments("--window-size=1920,1080");
			options.addArguments("--no-sandbox");
			options.addArguments("--disable-extensions");
			options.addArguments("--dns-prefetch-disable");
			options.addArguments("--disable-gpu");
			options.setPageLoadStrategy(PageLoadStrategy.NORMAL);

		} else if (browser.equals("firefox")) {
			driver = BrowserFactory.getBrowser(browser);
		} else if (browser.equals("edge")) {
			driver = BrowserFactory.getBrowser(browser);
		}

		driver.manage().deleteAllCookies();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.get(baseURL);
	}

	@AfterClass(alwaysRun = true)
	public void tearDown() throws IOException {
		BrowserFactory.quitBrowser(driver);
	}

	public static String captureScreen(WebDriver driver, String screenShotName) throws IOException {
		TakesScreenshot ts = (TakesScreenshot) driver;
		File source = ts.getScreenshotAs(OutputType.FILE);
		String screenshotpath = System.getProperty("user.dir") + "/test-output/screenshots/" + screenShotName + ".png";
		File destination = new File(screenshotpath);
		try {
			FileUtils.copyFile(source, destination);
		} catch (IOException e) {
			System.out.println("Capture Failed " + e.getMessage());
		}
		//System.out.println("Captured screenshot successfully");
		return screenshotpath;
	}

	public static String randomestring() {
		String generatedstring = RandomStringUtils.randomAlphabetic(8);
		return (generatedstring);
	}

	public static String randomeNum() {
		String generatedString2 = RandomStringUtils.randomNumeric(7);
		return (generatedString2);
	}

	public static String randomeNum8() {
		String generatedString2 = RandomStringUtils.randomNumeric(8);
		return (generatedString2);
	}

	public static String DateMethod() {
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		// get current date time with Date()
		Date date = new Date();
		// Now format the date
		String date1 = dateFormat.format(date);
		return date1;
	}
}