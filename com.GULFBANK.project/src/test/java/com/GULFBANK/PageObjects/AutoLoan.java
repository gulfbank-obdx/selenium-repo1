package com.GULFBANK.PageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.GULFBANK.Base.TestBase;
import com.GULFBANK.Utilities.GenericUtilities;

public class AutoLoan extends TestBase {

	WebDriver ldriver;

	public AutoLoan(WebDriver rdriver) {
		ldriver = rdriver;
		PageFactory.initElements(rdriver, this);
	}

	@FindBy(how = How.XPATH, using = "(//div[@data-bind='text: $component.nls.quickLinks.title[productTile.data.id]'])[4]")
	@CacheLookup
	WebElement Autoloanlink;

	public void Autoloan() {
		GenericUtilities.click(Autoloanlink);
	}

}
