package com.GULFBANK.PageObjects;

import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.GULFBANK.Base.TestBase;
import com.GULFBANK.Utilities.GenericUtilities;

public class PersonalLoanPage extends TestBase {

	WebDriver ldriver;

	public PersonalLoanPage(WebDriver rdriver) {
		ldriver = rdriver;
		PageFactory.initElements(rdriver, this);

	}

	@FindBy(how = How.XPATH, using = "(//div[@data-bind='text: $component.nls.quickLinks.title[productTile.data.id]'])[5]")
	@CacheLookup
	WebElement Personalloanlink;

	@FindBy(how = How.XPATH, using = "//div[@id='oj-select-choice-applicantdocumentsides']")
	@CacheLookup
	WebElement documentslidelink;

	@FindBy(how = How.XPATH, using = "text:$component.nls.PremierAccount.Continue")
	@CacheLookup
	WebElement continuelink;

	@FindBy(how = How.XPATH, using = "//div[@id='oj-select-choice-Years3']")
	@CacheLookup
	WebElement longtermyear;

	@FindBy(how = How.XPATH, using = "(//div[@class='oj-select-choice'])[2]")
	@CacheLookup
	WebElement longtermymonth;

	@FindBy(how = How.XPATH, using = "//oj-select-one[@options='[[$component.categoryList]]']")
	@CacheLookup
	WebElement selectDocumentType;

	@FindBy(how = How.XPATH, using = "//input[@id='Days72|input']")
	@CacheLookup
	WebElement longtermdays;

	@FindBy(how = How.XPATH, using = "(//div[@class='oj-select-choice'])[3]")
	@CacheLookup
	WebElement purposeofLoan;

	@FindBy(how = How.XPATH, using = "//oj-collapsible[@id='loandetailsection']")
	@CacheLookup
	WebElement loanDetailsCollapseable;

	@FindBy(how = How.XPATH, using = "//oj-collapsible[@id='loanamortizationsection']")
	@CacheLookup
	WebElement loanAmortizationCollapseable;

	public void PersonalLoan(String actual, String expected) throws Throwable {
		try {
			GenericUtilities.click(Personalloanlink);
			GenericUtilities.printExtentPassLog(actual, expected);
		} catch (Exception e) {
			GenericUtilities.printExtentFailLog(expected, e);
		}
	}

	public void documentslidelink() {
		documentslidelink.click();
		GenericUtilities.sleep(2);
		// documentslidelink.sendKeys(Keys.ARROW_DOWN);
		GenericUtilities.sleep(2);
		documentslidelink.sendKeys(Keys.ARROW_DOWN);
		GenericUtilities.sleep(2);
		documentslidelink.sendKeys(Keys.ENTER);
	}

	public void continue1() {
		GenericUtilities.click(continuelink);
	}

	public void longtermyear(String actual, String expected) throws Throwable {
		try {
			GenericUtilities.click(longtermyear);
			longtermyear.sendKeys(Keys.ARROW_DOWN);
			longtermyear.sendKeys(Keys.ENTER);
			GenericUtilities.printExtentPassLog(actual, expected);
		} catch (Exception e) {
			GenericUtilities.printExtentFailLog(expected, e);
		}
	}

	public void longtermmonths() {
		longtermymonth.click();
		GenericUtilities.sleep(2);
		longtermymonth.sendKeys(Keys.ARROW_DOWN);
		GenericUtilities.sleep(2);
		longtermymonth.sendKeys(Keys.ARROW_DOWN);
		GenericUtilities.sleep(2);
		longtermymonth.sendKeys(Keys.ARROW_DOWN);
		GenericUtilities.sleep(2);
		longtermymonth.sendKeys(Keys.ENTER);
	}

	public void selectDocType(String actual, String expected) throws Throwable {
		try {
			GenericUtilities.click(selectDocumentType);
			selectDocumentType.sendKeys(Keys.ARROW_DOWN);
			selectDocumentType.sendKeys(Keys.ARROW_DOWN);
			selectDocumentType.sendKeys(Keys.ENTER);
			GenericUtilities.printExtentPassLog(actual, expected);
		} catch (Exception e) {
			GenericUtilities.printExtentFailLog(expected, e);
		}
	}

	public void longtermdays() {
		longtermdays.click();
		GenericUtilities.sleep(2);
		longtermdays.sendKeys("5");
		GenericUtilities.sleep(2);
		longtermdays.sendKeys(Keys.ENTER);
	}

	public void purposeofLoanAccount() {
		try {
			GenericUtilities.click(purposeofLoan);
			GenericUtilities.sleep(2);
			purposeofLoan.sendKeys(Keys.ARROW_DOWN);
			GenericUtilities.sleep(2);
			purposeofLoan.sendKeys(Keys.ARROW_DOWN);
			GenericUtilities.sleep(2);
			purposeofLoan.sendKeys(Keys.ENTER);
		} catch (ElementNotInteractableException e) {
			GenericUtilities.click(purposeofLoan);
			GenericUtilities.sleep(2);
			purposeofLoan.sendKeys(Keys.ARROW_DOWN);
			GenericUtilities.sleep(2);
			purposeofLoan.sendKeys(Keys.ARROW_DOWN);
			GenericUtilities.sleep(2);
			purposeofLoan.sendKeys(Keys.ENTER);
		}
	}

	public void loanDetailsCollapse(String actual, String expected) throws Throwable {
		try {
			if (loanDetailsCollapseable.getAttribute("class").contains("collapsed")) {
				GenericUtilities.click(loanDetailsCollapseable);
			}
			GenericUtilities.printExtentPassLog(actual, expected);
		} catch (Exception e) {
			GenericUtilities.printExtentFailLog(expected, e);
		}
	}

	public void loanAmortizationCollapse() {
		if (loanAmortizationCollapseable.getAttribute("class").contains("collapsed")) {
			loanAmortizationCollapseable.click();
		}
	}
}