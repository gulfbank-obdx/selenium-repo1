package com.GULFBANK.PageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.GULFBANK.Base.TestBase;
import com.GULFBANK.Utilities.GenericUtilities;

public class Creditcards extends TestBase {

	WebDriver ldriver;

	public Creditcards(WebDriver rdriver) {
		ldriver = rdriver;
		PageFactory.initElements(rdriver, this);

	}

	@FindBy(how = How.XPATH, using = "(//div[@data-bind='text: $component.nls.quickLinks.title[productTile.data.id]'])[3]")
	@CacheLookup
	WebElement creditcardlink;

	@FindBy(how = How.XPATH, using = "(//label[@class='oj-button-label'])[1]")
	@CacheLookup
	WebElement cardinternationally;

	@FindBy(how = How.XPATH, using = "(//label[@class='oj-button-label'])[3]")
	@CacheLookup
	WebElement transferlink;

	@FindBy(how = How.XPATH, using = "(//label[@class='oj-button-label'])[5]")
	@CacheLookup
	WebElement Addoncard;

	public void creditcard() {
		GenericUtilities.click(creditcardlink);
	}

	public void cardinternationally() {
		GenericUtilities.click(cardinternationally);
	}

	public void transferlink() {
		GenericUtilities.click(transferlink);
	}

	public void Addoncard() {
		GenericUtilities.click(Addoncard);
	}
}
