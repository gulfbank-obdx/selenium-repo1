package com.GULFBANK.PageObjects;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.GULFBANK.Utilities.GenericUtilities;

public class ProductCategoriesPage {

	WebDriver driver;

	public ProductCategoriesPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	final String titleXpath = "//p[@class='title']";
	@FindBy(how = How.XPATH, using = titleXpath)
	@CacheLookup
	WebElement title;

	By categories = By.xpath("//div[@data-bind='text: $component.nls.quickLinks.title[productTile.data.id]']");

	By productImages = By.xpath("//img[@class='icons']");

	final String knowMoreSavAccXpath = "(//div[@class='icon-title_knowMore'])[1]";
	final String knowMoreXpath = "(//div[@class='icon-title_knowMore'])";
	By knowMore = By.xpath("//div[@class='icon-title_knowMore']");

	final String homeXpath = "(//span[@data-bind='text:$data.label'])[1]";
	By home = By.xpath(homeXpath);

	public WebElement getTitle() {
		return GenericUtilities.waitForElementUsingFluedWait(titleXpath);
	}

	public WebElement getHomeHyperlink() {
		return GenericUtilities.waitForElementUsingFluedWait(homeXpath);
	}

	public WebElement getKnowMoreSavingsAccount() {
		return GenericUtilities.waitForElementUsingFluedWait(knowMoreSavAccXpath);
	}

	public List<String> getCategoryList() {
		GenericUtilities.elementVisible(categories);
		List<WebElement> categoryList = driver.findElements(categories);
		List<String> categoryName = new ArrayList<String>();
		Iterator<WebElement> itr = categoryList.iterator();
		while (itr.hasNext()) {
			String text = itr.next().getText();
			categoryName.add(text);
		}
		return categoryName;
	}

	public List<WebElement> getImageList() {
		GenericUtilities.elementVisible(productImages);
		List<WebElement> imagelist = driver.findElements(productImages);
		return imagelist;
	}

	public List<WebElement> getKnowMoreList() {
		GenericUtilities.elementVisible(productImages);
		List<WebElement> imagelist = driver.findElements(knowMore);
		return imagelist;
	}

	public WebElement getKnowMore(String additionalXpath) {
		return GenericUtilities.waitForElementUsingFluedWait(knowMoreXpath.concat(additionalXpath));
	}
}