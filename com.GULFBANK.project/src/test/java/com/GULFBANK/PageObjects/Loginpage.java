package com.GULFBANK.PageObjects;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.GULFBANK.Utilities.GenericUtilities;

public class Loginpage {

	WebDriver driver;

	public Loginpage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.XPATH, using = "//*[@id=\"filmStrip\"]/div[4]/div/div/div[1]/div/a/div/div[2]/div[1]")
	@CacheLookup
	WebElement savinglink;

	@FindBy(how = How.XPATH, using = "(//span[@data-bind='text:$component.nls.PageSection.Continueasguest'])")
	@CacheLookup
	WebElement continueguestbtn;

	@FindBy(how = How.XPATH, using = "(//span[@data-bind='text:$component.nls.BeforeyouProceed.Continue'])")
	@CacheLookup
	WebElement continuebtn;

	@FindBy(how = How.XPATH, using = "//input[@type='checkbox']")
	@CacheLookup
	List<WebElement> checkBoxes;

	@FindBy(how = How.XPATH, using = "(//input[@class='oj-inputtext-input oj-text-field-input oj-component-initnode'])[1]")
	@CacheLookup
	WebElement emailinputtxt;

	@FindBy(how = How.XPATH, using = "(//input[@class='oj-inputtext-input oj-text-field-input oj-component-initnode'])[2]")
	@CacheLookup
	WebElement mobileinputtxt;

	@FindBy(how = How.XPATH, using = "(//span[@data-bind='text:$component.nls.PremierCheckingAccount.Continue'])")
	@CacheLookup
	WebElement continubt1;

	@FindBy(how = How.XPATH, using = "//div[@class='oj-message oj-message-error']/span//following-sibling::div/span")
	@CacheLookup
	List<WebElement> loginError;

	@FindBy(how = How.XPATH, using = "(//input[@class='oj-inputpassword-input oj-text-field-input oj-component-initnode'])")
	@CacheLookup
	WebElement Otpsentemail;

	@FindBy(how = How.XPATH, using = "(//span[@data-bind='text:$component.nls.PremierCheckingAccount.verify'])")
	@CacheLookup
	WebElement verifylnk;

	@FindBy(how = How.XPATH, using = "(//input[@class='oj-inputpassword-input oj-text-field-input oj-component-initnode'])[2]")
	@CacheLookup
	WebElement Otpsentmobile;

	@FindBy(how = How.XPATH, using = "(//span[@data-bind='text:$component.nls.PremierCheckingAccount.verify'])[2]")
	@CacheLookup
	WebElement verifyotp;

	@FindBy(how = How.XPATH, using = "(//span[@data-bind='text:$component.nls.PremierCheckingAccount.Continue'])")
	@CacheLookup
	WebElement continueotp;

	@FindBy(how = How.XPATH, using = "(//span[@data-bind='text:$component.nls.StartNewapplication'])")
	@CacheLookup
	WebElement Startnewapplication;

	@FindBy(how = How.XPATH, using = "//a[@class='oj-select-arrow oj-component-icon oj-clickable-icon-nocontext oj-select-open-icon']")
	@CacheLookup
	WebElement idtypedropdown;

	@FindBy(how = How.XPATH, using = "//div[@class='oj-listbox-result-label']")
	@CacheLookup
	List<WebElement> idTypeDropdownValues;

	@FindBy(how = How.XPATH, using = "//input[@id='civilID|input']")
	@CacheLookup
	WebElement civilIdTextbox;

	@FindBy(how = How.XPATH, using = "(//span[@data-bind='text:$component.nls.PremierAccount.Continue'])")
	@CacheLookup
	WebElement continueAfterIdType;

	@FindBy(how = How.XPATH, using = "(//*[@id='oj-listbox-result-label-4'])")
	@CacheLookup
	WebElement documentsidedrpdwn;

	@FindBy(how = How.XPATH, using = "//oj-select-one[@id='applicantdocumentsides']")
	@CacheLookup
	WebElement civilIdDocSide;

	@FindBy(how = How.XPATH, using = "//oj-select-one[@id='applicantcountrydropdown']")
	@CacheLookup
	WebElement passportCountry;

	@FindBy(how = How.XPATH, using = "(//input[@title='Search field'])[1]")
	@CacheLookup
	WebElement countrySearch;

	@FindBy(how = How.XPATH, using = "(//div[@class='oj-filepicker-icon oj-fwk-icon-plus oj-fwk-icon'])")
	@CacheLookup
	WebElement selectordropfilehere;

	@FindBy(how = How.XPATH, using = "(//span[@data-bind='text:$component.nls.PremierAccount.Continue'])")
	@CacheLookup
	WebElement continue2;

	@FindBy(how = How.XPATH, using = "//span[@data-bind='text:$component.nls.Continue']")
	@CacheLookup
	WebElement mobnoverifiedcontinue;

	@FindBy(how = How.XPATH, using = "(//div[@class='oj-select-choice'])[1]")
	@CacheLookup
	WebElement titledropdown;

	@FindBy(how = How.XPATH, using = "//input[@id='applicantNameInAR|input']")
	@CacheLookup
	WebElement applicanttxt;

	@FindBy(how = How.XPATH, using = "(//div[@id='oj-select-choice-MartialStatusOptional91'])")
	@CacheLookup
	WebElement maritalstatusdrpdown;

	@FindBy(how = How.XPATH, using = "(//div[@id='oj-select-choice-Citizenship36'])")
	@CacheLookup
	WebElement Citizenshipdrpdown;

	@FindBy(how = How.XPATH, using = "(//input[@class = 'oj-inputtext-input oj-text-field-input oj-component-initnode'])[6]")
	@CacheLookup
	WebElement nameasinpassport;

	@FindBy(how = How.XPATH, using = "(//div[@id='oj-select-choice-otherNationalities'])")
	@CacheLookup
	WebElement othernationalitesdrpdown;

	@FindBy(how = How.XPATH, using = "(//div[@id='oj-select-choice-countryOfBirth'])")
	@CacheLookup
	WebElement countryofbirthdropdown;

	@FindBy(how = How.XPATH, using = "(//div[@id='oj-select-choice-children'])")
	@CacheLookup
	WebElement childrendrpdown;

	@FindBy(how = How.XPATH, using = "(//div[@id='oj-select-choice-firstDegreeRelatives'])")
	@CacheLookup
	WebElement firstdegreerelativedrpdown;

	@FindBy(how = How.XPATH, using = "(//div[@id='oj-select-choice-specialNeedsFlag'])")
	@CacheLookup
	WebElement flagdrpdown;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Contact Details')]")
	@CacheLookup
	WebElement contactdetials;

	@FindBy(how = How.XPATH, using = "//div[contains(text(),'Add Permanent Address')]")
	@CacheLookup
	WebElement addaddressbutton;

	@FindBy(how = How.XPATH, using = "//input[@id='address|input']")
	@CacheLookup
	WebElement addaddrestxt;

	@FindBy(how = How.XPATH, using = "(//span[@class='icon-search'])")
	@CacheLookup
	WebElement searchicon;

	@FindBy(how = How.XPATH, using = "(//input[@class='oj-inputtext-input oj-text-field-input oj-component-initnode'])[11]")
	@CacheLookup
	WebElement housenotxt;

	@FindBy(how = How.XPATH, using = "(//input[@class='oj-inputtext-input oj-text-field-input oj-component-initnode'])[12]")
	@CacheLookup
	WebElement buildingnametxt;

	@FindBy(how = How.XPATH, using = "(//input[@class='oj-inputtext-input oj-text-field-input oj-component-initnode'])[13]")
	@CacheLookup
	WebElement streettxt;

	@FindBy(how = How.XPATH, using = "(//input[@class='oj-inputtext-input oj-text-field-input oj-component-initnode'])[14]")
	@CacheLookup
	WebElement localitytxt;

	@FindBy(how = How.XPATH, using = "(//input[@class='oj-inputtext-input oj-text-field-input oj-component-initnode'])[15]")
	@CacheLookup
	WebElement zipcodetxt;

	@FindBy(how = How.XPATH, using = "(//span[@data-bind='text:$component.nls.Address.Add'])")
	@CacheLookup
	WebElement addbtn;

	@FindBy(how = How.XPATH, using = "//span[@data-bind='text:$component.nls.PremierAccount.Continue']")
	@CacheLookup
	WebElement continuebutton;

	@FindBy(how = How.XPATH, using = "//input[@value='NonUS']")
	@CacheLookup
	WebElement fatcaStatus;

	@FindBy(how = How.XPATH, using = "(//input[@name='usCitizen'])[2]")
	@CacheLookup
	WebElement USCitizen;

	@FindBy(how = How.XPATH, using = "(//input[@name='usResidency'])[2]")
	@CacheLookup
	WebElement USResidency;

	@FindBy(how = How.XPATH, using = "(//input[@name='usTaxResidency'])[2]")
	@CacheLookup
	WebElement USTaxResidency;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Skip this Step')]")
	@CacheLookup
	WebElement skipthisstep;

	@FindBy(how = How.XPATH, using = "(//span[@data-bind='text:$component.nls.ProductsList.Apply'])[1]")
	@CacheLookup
	WebElement applyregularsaving;

	@FindBy(how = How.XPATH, using = "//oj-select-one[@id='employmentTypedp']")
	@CacheLookup
	WebElement Emptyedrpdown;

	@FindBy(how = How.XPATH, using = "//div[@id='oj-select-choice-academicQualifications']")
	@CacheLookup
	WebElement academicqualifcationdrpdown;

	@FindBy(how = How.XPATH, using = "(//div[@id='oj-select-choice-profession'])[1]")
	@CacheLookup
	WebElement professiondrpdown;

	@FindBy(how = How.XPATH, using = "//div[@id='oj-select-choice-employerName']")
	@CacheLookup
	WebElement empnamedrpdown;

	@FindBy(how = How.XPATH, using = "//div[contains(text(),'Add Work Address')]")
	@CacheLookup
	WebElement addworkaddress;

	@FindBy(how = How.XPATH, using = "//input[@id='FloorBayNo64|input']")
	@CacheLookup
	WebElement floorbayddress;

	@FindBy(how = How.XPATH, using = "//input[@id='Building89|input']")
	@CacheLookup
	WebElement buildingadd;

	@FindBy(how = How.XPATH, using = "//input[@id='AdditionalDetails47|input']")
	@CacheLookup
	WebElement additionaldetials;

	@FindBy(how = How.XPATH, using = "(//input[@id='ZipCode56|input'])[1]")
	@CacheLookup
	WebElement zipcode;

	@FindBy(how = How.XPATH, using = "//span[@data-bind='text:$component.nls.Address.Add']")
	@CacheLookup
	WebElement Addbutton;

	@FindBy(how = How.XPATH, using = "//input[@id='employmentDate|input']")
	@CacheLookup
	WebElement Empdate;

	@FindBy(how = How.XPATH, using = "//div[@id='oj-select-choice-natureOfEmployerActivity']")
	@CacheLookup
	WebElement Natureofmepl;

	@FindBy(how = How.XPATH, using = "(//span[@class='oj-radiocheckbox-icon'])[1]")
	@CacheLookup
	WebElement salarycheckbox;

	@FindBy(how = How.XPATH, using = "//input[@id='mainMonthlyIncome|input']")
	@CacheLookup
	WebElement incomesalarytxt;

	@FindBy(how = How.XPATH, using = "//input[@id='otherIncomeAmount|input']")
	@CacheLookup
	WebElement otherincometxt;

	@FindBy(how = How.XPATH, using = "//span[@data-bind='text:$component.nls.PremierAccount.Continue']")
	@CacheLookup
	WebElement contbtn;

	@FindBy(how = How.XPATH, using = "(//span[@data-bind='text:$component.nls.ProductsList.Apply'])[1]")
	@CacheLookup
	WebElement producListOne;

	@FindBy(how = How.XPATH, using = "//input[@id='nameOnCard|input']")
	@CacheLookup
	WebElement nameoncard;

	@FindBy(how = How.XPATH, using = "//div[@id='oj-select-choice-DebitCardDelivery']")
	@CacheLookup
	WebElement debitcarddelivery;

	@FindBy(how = How.XPATH, using = "//div[@id='oj-select-choice-debitBranch']")
	@CacheLookup
	WebElement branchLocation;

	@FindBy(how = How.XPATH, using = "//div[@id='oj-select-choice-ntea']")
	@CacheLookup
	WebElement natureOfExptdTrn;

	@FindBy(how = How.XPATH, using = "//div[@id='oj-select-choice-NoOfLeaves']")
	@CacheLookup
	WebElement noofleaves;

	@FindBy(how = How.XPATH, using = "//div[@id='oj-select-choice-CheckBookDelivery']")
	@CacheLookup
	WebElement checkbookdelivery;

	@FindBy(how = How.XPATH, using = "//div[@id='oj-select-choice-pop']")
	@CacheLookup
	WebElement purposeofopening;

	@FindBy(how = How.XPATH, using = "(//input[@type='radio'])[4]")
	@CacheLookup
	WebElement aobradiobankaccount;

	@FindBy(how = How.XPATH, using = "(//input[@name='aob'])[2]")
	@CacheLookup
	WebElement accountWithAnotherBank;

	@FindBy(how = How.XPATH, using = "//div[@id='oj-select-choice-ntea']")
	@CacheLookup
	WebElement nattxn;

	@FindBy(how = How.XPATH, using = "//div[@id='oj-select-choice-sow']")
	@CacheLookup
	WebElement sourcewealth;

	@FindBy(how = How.XPATH, using = "//div[@id='oj-select-choice-pep']")
	@CacheLookup
	WebElement pep;

	@FindBy(how = How.XPATH, using = "//span[@data-bind='text:$component.nls.PremierAccount.Continue']")
	@CacheLookup
	WebElement cntbtn;

	@FindBy(how = How.XPATH, using = "//input[@type='checkbox']")
	@CacheLookup
	WebElement agreeradionbtn;

	@FindBy(how = How.XPATH, using = "//span[@data-bind='text:$component.nls.TosHeader.UploadSignature']")
	@CacheLookup
	WebElement uploadbtn;

	@FindBy(how = How.XPATH, using = "//span[@data-bind='text:$component.nls.DocumentUploading.SelectorDropFilesHere']")
	@CacheLookup
	WebElement selectFilesHere;

	@FindBy(how = How.XPATH, using = "//div[contains(text(),'Upload Signature Here')]")
	@CacheLookup
	WebElement uploadbtn1;

	@FindBy(how = How.XPATH, using = "//span[@data-bind='text:$component.nls.TosHeader.Confirm']")
	@CacheLookup
	WebElement confirmbtn;

	@FindBy(how = How.XPATH, using = "//span[@data-bind='text:$component.nls.SubmitConfirmation.DownloadForm']")
	@CacheLookup
	WebElement submitconfirm;

	@FindBy(how = How.XPATH, using = "(//input[@type='checkbox'])[1]")
	@CacheLookup
	WebElement accountStmt;

	@FindBy(how = How.XPATH, using = "//div[@data-bind='text:$component.originationData.originationPayload.applicationId']")
	@CacheLookup
	WebElement applicationNumber;

	@FindBy(how = How.XPATH, using = "//a[@title='Track your Application']")
	@CacheLookup
	WebElement trackAppNumber;

	public void savingaccount(String actual, String expected) throws Throwable {
		try {
			GenericUtilities.click(savinglink);
			GenericUtilities.printExtentPassLog(actual, expected);
		} catch (Exception e) {
			GenericUtilities.printExtentFailLog(expected, e);
		}
	}

	public void continueguest(String actual, String expected) throws Throwable {
		try {
			GenericUtilities.sleep(3);
			GenericUtilities.click(continueguestbtn);
			GenericUtilities.printExtentPassLog(actual, expected);
		} catch (Exception e) {
			GenericUtilities.printExtentFailLog(expected, e);
		}
	}

	public void selectCheckBoxes(String actual, String expected) throws Throwable {
		try {
			for (WebElement checkbox : checkBoxes) {
				checkbox.click();
			}
			GenericUtilities.printExtentPassLog(actual, expected);
		} catch (Exception e) {
			GenericUtilities.printExtentFailLog(expected, e);
		}
	}

	public void continuebtn(String actual, String expected) throws Throwable {
		try {
			GenericUtilities.click(continuebtn);
			GenericUtilities.printExtentPassLog(actual, expected);
		} catch (Exception e) {
			GenericUtilities.printExtentFailLog(expected, e);
		}
	}

	public void emailinputtxt(String email, String actual, String expected) throws Throwable {
		try {
			GenericUtilities.entertext(emailinputtxt, email);
			GenericUtilities.printExtentPassLog(actual, expected);
		} catch (Exception e) {
			GenericUtilities.printExtentFailLog(expected, e);
		}
	}

	public void mobileinputtxt(String mobilenumber, String actual, String expected) throws Throwable {
		try {
			GenericUtilities.entertext(mobileinputtxt, mobilenumber);
			GenericUtilities.printExtentPassLog(actual, expected);
		} catch (Exception e) {
			GenericUtilities.printExtentFailLog(expected, e);
		}
	}

	public void continubt1(String actual, String expected) throws Throwable {
		try {
			GenericUtilities.click(continubt1);
			GenericUtilities.printExtentPassLog(actual, expected);
		} catch (Exception e) {
			GenericUtilities.printExtentFailLog(expected, e);
		}
	}

	public Boolean loginErrorCheck() {
		Boolean flag = null;
		List<String> actualErrorTexts = new ArrayList<String>();
		List<String> expectedErrorTexts = new ArrayList<String>();
		expectedErrorTexts.add("Please enter a valid email address");
		expectedErrorTexts.add("Enter 6 or more characters, not fewer.");
		expectedErrorTexts.add("Invalid mobile number");
		for (WebElement err : loginError) {
			if (err.isDisplayed()) {
				actualErrorTexts.add(err.getText());
			} else {
				flag = false;
			}
		}
		if (actualErrorTexts.equals(expectedErrorTexts)) {
			flag = true;
		}
		return flag;
	}

	public void Otpsentemail(String mobile, String actual, String expected) throws Throwable {
		try {
			GenericUtilities.entertext(Otpsentemail, mobile);
			GenericUtilities.printExtentPassLog(actual, expected);
		} catch (Exception e) {
			GenericUtilities.printExtentFailLog(expected, e);
		}
	}

	public void verifylnk(String actual, String expected) throws Throwable {
		try {
			GenericUtilities.click(verifylnk);
			GenericUtilities.printExtentPassLog(actual, expected);
		} catch (Exception e) {
			GenericUtilities.printExtentFailLog(expected, e);
		}
	}

	public void Otpsentmobile(String otpmobile, String actual, String expected) throws Throwable {
		try {
			GenericUtilities.entertext(Otpsentmobile, otpmobile);
			GenericUtilities.printExtentPassLog(actual, expected);
		} catch (Exception e) {
			GenericUtilities.printExtentFailLog(expected, e);
		}
	}

	public void verifyotp(String actual, String expected) throws Throwable {
		try {
			GenericUtilities.click(verifyotp);
			GenericUtilities.printExtentPassLog(actual, expected);
		} catch (Exception e) {
			GenericUtilities.printExtentFailLog(expected, e);
		}
	}

	public void continueotp(String actual, String expected) throws Throwable {
		try {
			GenericUtilities.click(continueotp);
			GenericUtilities.printExtentPassLog(actual, expected);
		} catch (Exception e) {
			GenericUtilities.printExtentFailLog(expected, e);
		}
	}

	public void Startnewapplication() {
		Startnewapplication.click();
	}

	public void idtypedropdown(String actual, String expected) throws Throwable {
		try {
			GenericUtilities.click(idtypedropdown);
			GenericUtilities.printExtentPassLog(actual, expected);
		} catch (Exception e) {
			GenericUtilities.printExtentFailLog(expected, e);
		}
	}

	public void selectIdTypeValue(String idValue, String actual, String expected) throws Throwable {
		try {
			for (WebElement idType : idTypeDropdownValues) {
				String text = idType.getAttribute("id");
				if (text.contains(idValue)) {
					idType.click();
				}
			}
			GenericUtilities.printExtentPassLog(actual, expected);
		} catch (Exception e) {
			GenericUtilities.printExtentFailLog(expected, e);
		}
	}

	public void enterCivilId(String civilId, String actual, String expected) throws Throwable {
		try {
			GenericUtilities.entertext(civilIdTextbox, civilId);
			GenericUtilities.printExtentPassLog(actual, expected);
		} catch (Exception e) {
			GenericUtilities.printExtentFailLog(expected, e);
		}
	}

	public void continueAfterIdType(String actual, String expected) throws Throwable {
		try {
			GenericUtilities.click(continueAfterIdType);
			GenericUtilities.printExtentPassLog(actual, expected);
			GenericUtilities.sleep(4);
		} catch (Exception e) {
			GenericUtilities.printExtentFailLog(expected, e);
		}
	}

	public void documentsidedrpdwn() {
		Select drop = new Select(documentsidedrpdwn);
		GenericUtilities.sleep(3);
		drop.selectByVisibleText("Back");
	}

	public void selectordropfilehere(String actual, String expected) throws Throwable {
		try {
			GenericUtilities.click(selectordropfilehere);
			GenericUtilities.printExtentPassLog(actual, expected);
		} catch (Exception e) {
			GenericUtilities.printExtentFailLog(expected, e);
		}
	}

	public void continue2() {
		continue2.click();
	}

	public void mobnoverifiedcontinue(String actual, String expected) throws Throwable {
		try {
			GenericUtilities.click(mobnoverifiedcontinue);
			GenericUtilities.printExtentPassLog(actual, expected);
		} catch (Exception e) {
			GenericUtilities.printExtentFailLog(expected, e);
		}
	}

	public void titledropdown(String actual, String expected) throws Throwable {
		try {
			GenericUtilities.click(titledropdown);
			titledropdown.sendKeys(Keys.ARROW_DOWN);
			titledropdown.sendKeys(Keys.ARROW_DOWN);
			titledropdown.sendKeys(Keys.ENTER);
			GenericUtilities.printExtentPassLog(actual, expected);
		} catch (ElementNotInteractableException e) {
			GenericUtilities.printExtentFailLog(expected, e);
		}

	}

	public void applicanttxt(String name) {
		GenericUtilities.entertext(applicanttxt, name);
		GenericUtilities.sleep(2);
		applicanttxt.sendKeys(Keys.TAB);
	}

	public void maritalstatusdrpdown(String actual, String expected) throws Throwable {
		try {
			GenericUtilities.click(maritalstatusdrpdown);
			maritalstatusdrpdown.sendKeys(Keys.ARROW_DOWN);
			maritalstatusdrpdown.sendKeys(Keys.ARROW_DOWN);
			maritalstatusdrpdown.sendKeys(Keys.ENTER);
			GenericUtilities.printExtentPassLog(actual, expected);
		} catch (ElementNotInteractableException e) {
			GenericUtilities.printExtentFailLog(expected, e);
		}
	}

	public void civilIdDocSide(String actual, String expected) throws Throwable {
		try {
			GenericUtilities.click(civilIdDocSide);
			civilIdDocSide.sendKeys(Keys.ARROW_DOWN);
			civilIdDocSide.sendKeys(Keys.ARROW_DOWN);
			civilIdDocSide.sendKeys(Keys.ENTER);
			GenericUtilities.printExtentPassLog(actual, expected);
		} catch (Exception e) {
			GenericUtilities.printExtentFailLog(expected, e);
		}
	}

	public void passportCountry(String countryName, String actual, String expected) throws Throwable {
		GenericUtilities.sleep(3);
		try {
			GenericUtilities.click(passportCountry);
			passportCountry.sendKeys("");
			GenericUtilities.sleep(2);
			countrySearch.clear();
			countrySearch.sendKeys(countryName);
			GenericUtilities.sleep(2);
			countrySearch.sendKeys(Keys.ARROW_DOWN);
			countrySearch.sendKeys(Keys.ENTER);
			GenericUtilities.printExtentPassLog(actual, expected);
		} catch (Exception e) {
			GenericUtilities.printExtentFailLog(expected, e);
		}
	}

	public void Citizenshipdrpdown() {
		Citizenshipdrpdown.click();
		GenericUtilities.sleep(3);
		Citizenshipdrpdown.sendKeys(Keys.ARROW_DOWN);
		GenericUtilities.sleep(3);
		Citizenshipdrpdown.sendKeys(Keys.ENTER);
	}

	public void nameasinpassport(String name, String actual, String expected) throws Throwable {
		try {
			GenericUtilities.entertext(nameasinpassport, name);
			GenericUtilities.printExtentPassLog(actual, expected);
		} catch (Exception e) {
			GenericUtilities.printExtentFailLog(expected, e);
		}
	}

	public void childrenDropdown(String actual, String expected) throws Throwable {
		try {
			GenericUtilities.click(childrendrpdown);
			GenericUtilities.sleep(2);
			childrendrpdown.sendKeys(Keys.ARROW_DOWN);
			childrendrpdown.sendKeys(Keys.ARROW_DOWN);
			childrendrpdown.sendKeys(Keys.ENTER);
			GenericUtilities.printExtentPassLog(actual, expected);
		} catch (Exception e) {
			GenericUtilities.printExtentFailLog(expected, e);
		}
	}

	public void othernationalitesdrpdown() {
		GenericUtilities.click(othernationalitesdrpdown);
		GenericUtilities.sleep(3);
		othernationalitesdrpdown.sendKeys(Keys.ARROW_DOWN);
		GenericUtilities.sleep(3);
		othernationalitesdrpdown.sendKeys(Keys.ARROW_DOWN);
		GenericUtilities.sleep(3);
		othernationalitesdrpdown.sendKeys(Keys.ARROW_DOWN);
		GenericUtilities.sleep(3);
		othernationalitesdrpdown.sendKeys(Keys.ENTER);
	}

	public void countryofbirthdropdown() {
		GenericUtilities.click(countryofbirthdropdown);
		GenericUtilities.sleep(3);
		countryofbirthdropdown.sendKeys(Keys.ARROW_DOWN);
		GenericUtilities.sleep(3);
		countryofbirthdropdown.sendKeys(Keys.ENTER);
	}

	public void childrendrpdown() {
		GenericUtilities.click(childrendrpdown);
		GenericUtilities.sleep(3);
		childrendrpdown.sendKeys(Keys.ARROW_DOWN);
		GenericUtilities.sleep(3);
		childrendrpdown.sendKeys(Keys.ARROW_DOWN);
		GenericUtilities.sleep(3);
		childrendrpdown.sendKeys(Keys.ENTER);
		GenericUtilities.sleep(3);
	}

	public void firstdegreerelativedrpdown() {
		GenericUtilities.click(firstdegreerelativedrpdown);
		GenericUtilities.sleep(3);
		firstdegreerelativedrpdown.sendKeys(Keys.ARROW_DOWN);
		GenericUtilities.sleep(3);
		firstdegreerelativedrpdown.sendKeys(Keys.ARROW_DOWN);
		GenericUtilities.sleep(3);
		firstdegreerelativedrpdown.sendKeys(Keys.ENTER);
	}

	public void flagdrpdown() {
		GenericUtilities.click(flagdrpdown);
		GenericUtilities.sleep(3);
		flagdrpdown.sendKeys(Keys.ARROW_DOWN);
		GenericUtilities.sleep(3);
		flagdrpdown.sendKeys(Keys.ARROW_DOWN);
		GenericUtilities.sleep(3);
		flagdrpdown.sendKeys(Keys.ENTER);
	}

	public void contactdetials() {
		contactdetials.click();
	}

	public void addaddressbutton(String actual, String expected) throws Throwable {
		try {
			GenericUtilities.click(addaddressbutton);
			GenericUtilities.sleep(2);
			GenericUtilities.printExtentPassLog(actual, expected);
		} catch (Exception e) {
			GenericUtilities.printExtentFailLog(expected, e);
		}
	}

	public void addaddrestxt(String address) {
		addaddrestxt.click();
		addaddrestxt.sendKeys(address);
		GenericUtilities.sleep(3);
		addaddrestxt.sendKeys(Keys.ARROW_DOWN);
		addaddrestxt.sendKeys(Keys.ENTER);
	}

	public void searchicon() {
		GenericUtilities.click(searchicon);
	}

	public void housenotxt(String address, String actual, String expected) throws Throwable {
		try {
			GenericUtilities.entertext(housenotxt, address);
			GenericUtilities.printExtentPassLog(actual, expected);
		} catch (Exception e) {
			GenericUtilities.printExtentFailLog(expected, e);
		}
	}

	public void buildingnametxt(String address, String actual, String expected) throws Throwable {
		try {
			GenericUtilities.entertext(buildingnametxt, address);
			GenericUtilities.printExtentPassLog(actual, expected);
		} catch (Exception e) {
			GenericUtilities.printExtentFailLog(expected, e);
		}
	}

	public void streettxt(String address, String actual, String expected) throws Throwable {
		try {
			GenericUtilities.entertext(streettxt, address);
			GenericUtilities.printExtentPassLog(actual, expected);
		} catch (Exception e) {
			GenericUtilities.printExtentFailLog(expected, e);
		}
	}

	public void localitytxt(String address, String actual, String expected) throws Throwable {
		try {
			GenericUtilities.entertext(localitytxt, address);
			GenericUtilities.sleep(3);
			GenericUtilities.printExtentPassLog(actual, expected);
		} catch (Exception e) {
			GenericUtilities.printExtentFailLog(expected, e);
		}
	}

	public void zipcodetxt(String add, String actual, String expected) throws Throwable {
		try {
			GenericUtilities.entertext(zipcodetxt, add);
			GenericUtilities.sleep(2);
			zipcodetxt.sendKeys(Keys.TAB);
			GenericUtilities.printExtentPassLog(actual, expected);
		} catch (Exception e) {
			GenericUtilities.printExtentFailLog(expected, e);
		}
	}

	public void addbtn(String actual, String expected) throws Throwable {
		try {
			GenericUtilities.click(addbtn);
			GenericUtilities.sleep(2);
			GenericUtilities.printExtentPassLog(actual, expected);
		} catch (Exception e) {
			GenericUtilities.printExtentFailLog(expected, e);
		}
	}

	public void fatcaStatusRadioButton(String actual, String expected) throws Throwable {
		try {
			// GenericUtilities.click(fatcaStatus);
			fatcaStatus.click();
			GenericUtilities.printExtentPassLog(actual, expected);
		} catch (Exception e) {
			GenericUtilities.printExtentFailLog(expected, e);
		}
	}

	public void USCitizenRadioButton(String actual, String expected) throws Throwable {
		try {
			// GenericUtilities.click(USCitizen);
			USCitizen.click();
			GenericUtilities.printExtentPassLog(actual, expected);
		} catch (Exception e) {
			GenericUtilities.printExtentFailLog(expected, e);
		}
	}

	public void USResidencyRadioButton(String actual, String expected) throws Throwable {
		try {
			// GenericUtilities.click(USResidency);
			USResidency.click();
			GenericUtilities.printExtentPassLog(actual, expected);
		} catch (Exception e) {
			GenericUtilities.printExtentFailLog(expected, e);
		}
	}

	public void USTaxResidencyRadioButton(String actual, String expected) throws Throwable {
		try {
			// GenericUtilities.click(USTaxResidency);
			USTaxResidency.click();
			GenericUtilities.printExtentPassLog(actual, expected);
		} catch (Exception e) {
			GenericUtilities.printExtentFailLog(expected, e);
		}
	}

	public void contbtn(String actual, String expected) throws Throwable {
		try {
			GenericUtilities.click(continuebutton);
			GenericUtilities.printExtentPassLog(actual, expected);
		} catch (Exception e) {
			GenericUtilities.printExtentFailLog(expected, e);
		}
	}

	public void skipthisstep(String actual, String expected) throws Throwable {
		try {
			// GenericUtilities.click(skipthisstep);
			skipthisstep.click();
			GenericUtilities.printExtentPassLog(actual, expected);
		} catch (Exception e) {
			GenericUtilities.printExtentFailLog(expected, e);
		}
	}

	public void applyregularsaving() {
		applyregularsaving.click();
	}

	public void Emptyedrpdown(String actual, String expected) throws Throwable {
		try {
			GenericUtilities.click(Emptyedrpdown);
			Emptyedrpdown.sendKeys(Keys.ARROW_DOWN);
			Emptyedrpdown.sendKeys(Keys.ARROW_DOWN);
			Emptyedrpdown.sendKeys(Keys.ENTER);
			GenericUtilities.printExtentPassLog(actual, expected);
		} catch (Exception e) {
			GenericUtilities.printExtentFailLog(expected, e);
		}
	}

	public void academicqualifcationdrpdown(String actual, String expected) throws Throwable {
		try {
			GenericUtilities.click(academicqualifcationdrpdown);
			academicqualifcationdrpdown.sendKeys(Keys.ARROW_DOWN);
			academicqualifcationdrpdown.sendKeys(Keys.ARROW_DOWN);
			academicqualifcationdrpdown.sendKeys(Keys.ENTER);
			GenericUtilities.printExtentPassLog(actual, expected);
		} catch (Exception e) {
			GenericUtilities.printExtentFailLog(expected, e);
		}
	}

	public void professiondrpdown(String actual, String expected) throws Throwable {
		try {
			GenericUtilities.click(professiondrpdown);
			professiondrpdown.sendKeys(Keys.ARROW_DOWN);
			professiondrpdown.sendKeys(Keys.ARROW_DOWN);
			professiondrpdown.sendKeys(Keys.ENTER);
			GenericUtilities.printExtentPassLog(actual, expected);
		} catch (Exception e) {
			GenericUtilities.printExtentFailLog(expected, e);
		}
	}

	public void empnamedrpdown(String actual, String expected) throws Throwable {
		try {
			GenericUtilities.click(empnamedrpdown);
			empnamedrpdown.sendKeys(Keys.ARROW_DOWN);
			empnamedrpdown.sendKeys(Keys.ARROW_DOWN);
			empnamedrpdown.sendKeys(Keys.ENTER);
			GenericUtilities.printExtentPassLog(actual, expected);
		} catch (Exception e) {
			GenericUtilities.printExtentFailLog(expected, e);
		}
	}

	public void addworkaddress() {
		addworkaddress.click();
	}

	public void floorbayddress(String addr) {
		GenericUtilities.entertext(floorbayddress, addr);
	}

	public void buildingadd(String buldadd) {
		GenericUtilities.entertext(buildingadd, buldadd);
	}

	public void additionaldetials(String buldadd) {
		GenericUtilities.entertext(additionaldetials, buldadd);
	}

	public void zipcode() {
		zipcode.sendKeys("560066");
		GenericUtilities.sleep(2);
		zipcode.sendKeys(Keys.TAB);
		GenericUtilities.sleep(2);
	}

	public void zipcode11(String zipcode1) {
		zipcode.sendKeys(zipcode1);
		GenericUtilities.sleep(2);
		zipcode.sendKeys(Keys.TAB);
		GenericUtilities.sleep(2);
	}

	public void Addbutton() {
		Addbutton.click();
	}

	public void Empdate(String date, String actual, String expected) throws Throwable {
		try {
			GenericUtilities.entertext(Empdate, date);
			GenericUtilities.sleep(1);
			zipcodetxt.sendKeys(Keys.TAB);
			GenericUtilities.printExtentPassLog(actual, expected);
		} catch (Exception e) {
			GenericUtilities.printExtentFailLog(expected, e);
		}
	}

	public void Natureofmepl(String actual, String expected) throws Throwable {
		try {
			GenericUtilities.click(Natureofmepl);
			Natureofmepl.sendKeys(Keys.ARROW_DOWN);
			Natureofmepl.sendKeys(Keys.ENTER);
			GenericUtilities.printExtentPassLog(actual, expected);
		} catch (Exception e) {
			GenericUtilities.printExtentFailLog(expected, e);
		}
	}

	public void salarycheckbox(String actual, String expected) throws Throwable {
		try {
			GenericUtilities.click(salarycheckbox);
			Natureofmepl.sendKeys(Keys.ARROW_DOWN);
			Natureofmepl.sendKeys(Keys.ENTER);
			GenericUtilities.printExtentPassLog(actual, expected);
		} catch (Exception e) {
			GenericUtilities.printExtentFailLog(expected, e);
		}
	}

	public void incomesalarytxt(String income, String actual, String expected) throws Throwable {
		try {
			GenericUtilities.entertext(incomesalarytxt, income);
			GenericUtilities.printExtentPassLog(actual, expected);
		} catch (Exception e) {
			GenericUtilities.printExtentFailLog(expected, e);
		}
	}

	public void otherincometxt(String otherincome) {
		otherincometxt.sendKeys(otherincome);
	}

	public void contbtn3() {
		contbtn.click();
	}

	public void applyRegularSavingsAccount() {
		producListOne.click();
	}

	public void applyConsumerLoan(String actual, String expected) throws Throwable {
		producListOne.click();
		try {
			GenericUtilities.click(producListOne);
			GenericUtilities.printExtentPassLog(actual, expected);
		} catch (Exception e) {
			GenericUtilities.printExtentFailLog(expected, e);
		}
	}

	public void platinumChipCreditCard() {
		producListOne.click();
	}

	public void nameoncard() {
		nameoncard.sendKeys("Profinch");
	}

	public void nameoncard1(String cardname) {
		nameoncard.sendKeys(cardname);
	}

	public void debitcarddelivery() {
		debitcarddelivery.click();
		GenericUtilities.sleep(2);
		debitcarddelivery.sendKeys(Keys.ARROW_DOWN);
		debitcarddelivery.sendKeys(Keys.ARROW_DOWN);
		debitcarddelivery.sendKeys(Keys.ENTER);
	}

	public void branchLocation() {
		branchLocation.click();
		GenericUtilities.sleep(2);
		branchLocation.sendKeys(Keys.ARROW_DOWN);
		branchLocation.sendKeys(Keys.ARROW_DOWN);
		branchLocation.sendKeys(Keys.ENTER);
	}

	public void natureOfExptdtrn() {
		natureOfExptdTrn.click();
		GenericUtilities.sleep(2);
		natureOfExptdTrn.sendKeys(Keys.ARROW_DOWN);
		natureOfExptdTrn.sendKeys(Keys.ENTER);
	}

	public void noofleaves() {
		noofleaves.click();
		GenericUtilities.sleep(2);
		noofleaves.sendKeys(Keys.ARROW_DOWN);
		GenericUtilities.sleep(2);
		noofleaves.sendKeys(Keys.ARROW_DOWN);
		GenericUtilities.sleep(3);
		noofleaves.sendKeys(Keys.ENTER);
	}

	public void checkbookdelivery() {
		checkbookdelivery.click();
		GenericUtilities.sleep(2);
		checkbookdelivery.sendKeys(Keys.ARROW_DOWN);
		GenericUtilities.sleep(2);
		// checkbookdelivery.sendKeys(Keys.ARROW_DOWN);
		GenericUtilities.sleep(3);
		checkbookdelivery.sendKeys(Keys.ENTER);
	}

	public void purposeofopening() {
		purposeofopening.click();
		GenericUtilities.sleep(2);
		purposeofopening.sendKeys(Keys.ARROW_DOWN);
		GenericUtilities.sleep(2);
		purposeofopening.sendKeys(Keys.ARROW_DOWN);
		GenericUtilities.sleep(3);
		purposeofopening.sendKeys(Keys.ENTER);
	}

	public void aobradiobankaccount() {
		aobradiobankaccount.click();
	}

	public void accountWithAnotherBnk() {
		accountWithAnotherBank.click();
	}

	public void nattxn() {
		nattxn.click();
		GenericUtilities.sleep(2);
		nattxn.sendKeys(Keys.ARROW_DOWN);
		GenericUtilities.sleep(2);
		nattxn.sendKeys(Keys.ARROW_DOWN);
		GenericUtilities.sleep(2);
		GenericUtilities.sleep(2);
		nattxn.sendKeys(Keys.ENTER);
	}

	public void sourcewealth() {
		sourcewealth.click();
		GenericUtilities.sleep(2);
		sourcewealth.sendKeys(Keys.ARROW_DOWN);
		GenericUtilities.sleep(2);
		sourcewealth.sendKeys(Keys.ARROW_DOWN);
		GenericUtilities.sleep(2);
		sourcewealth.sendKeys(Keys.ENTER);
	}

	public void pep() {
		pep.click();
		GenericUtilities.sleep(2);
		pep.sendKeys(Keys.ARROW_DOWN);
		GenericUtilities.sleep(2);
		pep.sendKeys(Keys.ARROW_DOWN);
		GenericUtilities.sleep(2);
		pep.sendKeys(Keys.ENTER);
	}

	public void cntbtn() {
		cntbtn.click();
	}

	public void scrollTocntbtn() {
		GenericUtilities.scrolluntilByElementVisible(cntbtn);
		cntbtn.click();
	}

	public void agreeradionbtn(String actual, String expected) throws Throwable {
		agreeradionbtn.click();
		try {
			GenericUtilities.click(uploadbtn);
			GenericUtilities.printExtentPassLog(actual, expected);
		} catch (Exception e) {
			GenericUtilities.printExtentFailLog(expected, e);
		}
	}

	public void uploadbtn(String actual, String expected) throws Throwable {
		try {
			GenericUtilities.click(uploadbtn);
			GenericUtilities.printExtentPassLog(actual, expected);
		} catch (Exception e) {
			GenericUtilities.printExtentFailLog(expected, e);
		}
	}

	public void selectOrDropFilesHere() {
		selectFilesHere.click();
	}

	public void uploadbtn1(String actual, String expected) throws Throwable {
		try {
			GenericUtilities.click(uploadbtn1);
			GenericUtilities.printExtentPassLog(actual, expected);
		} catch (Exception e) {
			GenericUtilities.printExtentFailLog(expected, e);
		}
	}

	public void confirmbtn(String actual, String expected) throws Throwable {
		try {
			GenericUtilities.click(confirmbtn);
			GenericUtilities.printExtentPassLog(actual, expected);
		} catch (Exception e) {
			GenericUtilities.printExtentFailLog(expected, e);
		}
	}

	public void submitconfirm() {
		submitconfirm.click();
	}

	public void accountStatementCheckbox() {
		accountStmt.click();
	}

	public String getApplicationNumber() {
		return applicationNumber.getText();
	}

	public void trackApplication(String actual, String expected) throws Throwable {
		try {
			GenericUtilities.click(trackAppNumber);
			GenericUtilities.printExtentPassLog(actual, expected);
		} catch (Exception e) {
			GenericUtilities.printExtentFailLog(expected, e);
		}
	}
}
