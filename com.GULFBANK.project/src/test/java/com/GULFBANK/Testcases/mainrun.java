package com.GULFBANK.Testcases;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.testng.TestNG;

public class mainrun {

	static TestNG testng;

	public static void main(String[] args) throws IOException {

		System.out.println("hi");
		List<String> xmlFileList = new ArrayList<String>();
		xmlFileList.add(System.getProperty("user.dir") + "\\com.GULFBANK.project\\testng.xml");

		TestNG testng = new TestNG();
		testng.setTestSuites(xmlFileList);

		testng.setDefaultSuiteName("My Test Suite");
		testng.setDefaultTestName("My Test");
		testng.setOutputDirectory(
				System.getProperty("user.dir") + "\\com.GULFBANK.project\\test-output");

		testng.setTestClasses(new Class[] { com.GULFBANK.Testcases.SavingAccount.class });
		testng.run();
	}
}