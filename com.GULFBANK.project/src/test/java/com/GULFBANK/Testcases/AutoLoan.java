package com.GULFBANK.Testcases;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import com.GULFBANK.Base.TestBase;
import com.GULFBANK.PageObjects.Loginpage;
import com.GULFBANK.Utilities.GenericUtilities;

public class AutoLoan extends TestBase {

	@Test
	public void AutoLoans() throws AWTException {

		com.GULFBANK.PageObjects.AutoLoan auto = new com.GULFBANK.PageObjects.AutoLoan(driver);
		auto.Autoloan();
		com.GULFBANK.PageObjects.PersonalLoanPage pl = new com.GULFBANK.PageObjects.PersonalLoanPage(driver);
		Loginpage lp = new Loginpage(driver);

		JavascriptExecutor js = (JavascriptExecutor) driver;
		WebElement Element = driver.findElement(By.xpath("(//span[@class='oj-button-text'])[4]"));
		js.executeScript("arguments[0].scrollIntoView();", Element);
		logger.info("Step --> Scroll down to continue guest");
		GenericUtilities.sleep(2);

		//lp.continueguest();
		logger.info("Step-2-----> Click on Continue Guest button");
		GenericUtilities.sleep(2);

		//lp.continuebtn();
		logger.info("Step-3----> Click on Continue button");
		GenericUtilities.sleep(2);

		//lp.emailinputtxt(randomestring() + "@gmail.com");
		logger.info("Step-4 ---> Enter the text in the email field");
		GenericUtilities.sleep(2);

		//lp.mobileinputtxt(randomeNum8());
		logger.info("Step-5-----> Enter the mobile number");
		GenericUtilities.sleep(2);

		//lp.continubt1();
		logger.info("Step -6-----> click on continue");
		GenericUtilities.sleep(4);

		//lp.Otpsentemail("1111");
		logger.info("Step-7---> Enter the Otp number");
		GenericUtilities.sleep(2);

		//lp.verifylnk();
		logger.info("Step -8----> Click on Verify");
		GenericUtilities.sleep(2);

		//lp.Otpsentmobile("1111");
		logger.info("Step-9----> Enter mobile on otp");
		GenericUtilities.sleep(2);

		//lp.verifyotp();
		logger.info("Step-10 ---> Verify otp");
		GenericUtilities.sleep(5);

		//lp.continueotp();
		logger.info("Step-12---> Click on continue");
		GenericUtilities.sleep(5);

		//lp.mobnoverifiedcontinue();
		GenericUtilities.sleep(2);

		// lp.Startnewapplication();
		logger.info("Step-13----> Click on continue on mobile verified page");
		GenericUtilities.sleep(2);

		//lp.idtypedropdown();

		try {
			WebDriverWait wait1 = new WebDriverWait(driver, 60);
			WebElement reason = wait1.until(
					ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@id='oj-listbox-result-label-2']")));
			GenericUtilities.sleep(2);
			reason.click();

		} catch (StaleElementReferenceException e) {
			WebDriverWait wait1 = new WebDriverWait(driver, 60);
			WebElement reason = wait1.until(
					ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@id='oj-listbox-result-label-2']")));
			GenericUtilities.sleep(2);
			reason.click();
		}
		logger.info("Step-13----> select the drop down id type");
		GenericUtilities.sleep(2);

		pl.documentslidelink();
		GenericUtilities.sleep(2);

		/*
		 * List<WebElement> opt1 =
		 * driver.findElements(By.xpath("(//span[@class='oj-select-chosen'])[2]")); int
		 * s1 = opt1.size(); // Iterating through the list selecting the desired option
		 * for( int j = 0; j< opt1.size();j++){ // if the option is By Subject click
		 * that option if( opt1.get(j).getText().equals("Back")){ opt1.get(j).click();
		 * break; } }
		 */

		// lp.documentsidedrpdwn();
		logger.info("Step-14----> select the document side dropdown");
		GenericUtilities.sleep(8);

		//lp.selectordropfilehere();
		logger.info("Step-15-----> Select the drop file here");
		GenericUtilities.sleep(8);

		Robot robot = new Robot();
		robot.setAutoDelay(2000);

		StringSelection stringselection = new StringSelection(
				System.getProperty("user.dir") + "\\src\\test\\java\\com\\GULFBANK\\TestData\\Himanshu_Civil ID.jpg");

		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringselection, null);

		robot.setAutoDelay(2000);

		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_V);

		robot.keyRelease(KeyEvent.VK_CONTROL);
		robot.keyRelease(KeyEvent.VK_V);

		robot.setAutoDelay(2000);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);

		GenericUtilities.sleep(2);
		logger.info(" Step-16----> Impage uploaded sucessfully");

		lp.continue2();
		logger.info("Step-17----> Click on continue");
		GenericUtilities.sleep(5);

		GenericUtilities.sleep(5);
		lp.applicanttxt(randomestring());
		logger.info("Step -19----> Enter the name of the applicant");
		GenericUtilities.sleep(5);

		//lp.titledropdown();
		logger.info("Step-18---> Click on Title dropdown");
		GenericUtilities.sleep(5);

		//lp.maritalstatusdrpdown();
		logger.info(" Step-20----> Select the Marital Status drop down");
		GenericUtilities.sleep(5);

		lp.Citizenshipdrpdown();
		logger.info("Step-21---> Select the citizenship dropdown");
		GenericUtilities.sleep(5);

		//lp.nameasinpassport("Himanshu");
		logger.info("Step-22---> Enter the name as in Passport");
		GenericUtilities.sleep(3);

		lp.contactdetials();
		logger.info("Step-27---->Click on contact detials");
		GenericUtilities.sleep(3);

		//lp.addaddressbutton();
		logger.info("Step-28---> Click on Add address button");
		GenericUtilities.sleep(2);

		lp.addaddrestxt("Bangalore");
		logger.info("Step-30----> enter the Address");
		GenericUtilities.sleep(2);

		lp.searchicon();
		logger.info("Step-31---> Click on Search icon");
		GenericUtilities.sleep(2);

		//lp.housenotxt("316");
		logger.info("Step-32---> Enter House Number");
		GenericUtilities.sleep(3);

		//lp.buildingnametxt("Apartment");
		logger.info("Step-33----> Enter Building name");
		GenericUtilities.sleep(2);

		//lp.streettxt("Hoodi");
		logger.info("Step-34----> Enter Street text");
		GenericUtilities.sleep(2);

		//lp.localitytxt("Hoodi Circle");
		logger.info("Step-35----> Enter locality");
		GenericUtilities.sleep(2);

		//lp.zipcodetxt("560048");
		logger.info("Step-36---> Enter zip code");
		GenericUtilities.sleep(2);

		//lp.addbtn();
		logger.info("Step-37---->Click on Add button");
		GenericUtilities.sleep(3);

		//lp.contbtn();
		logger.info("Step-38---->Click on Continue button");
		GenericUtilities.sleep(8);

		//lp.Emptyedrpdown();
		logger.info("Step-41---> Select the Employment drop down");
		GenericUtilities.sleep(2);

		//lp.academicqualifcationdrpdown();
		logger.info("Step-42---> Select the academic qualifcation");
		GenericUtilities.sleep(2);

		//lp.professiondrpdown();
		logger.info("Step-43---> Select the profession");
		GenericUtilities.sleep(2);

		//lp.empnamedrpdown();
		logger.info("Step-44---> select the employee name dropdown");
		GenericUtilities.sleep(2);

		lp.addworkaddress();
		logger.info("Step-45----> Click on Work address");
		GenericUtilities.sleep(2);

		lp.floorbayddress("111");
		logger.info("Step-46---> Enter floor bay No");
		GenericUtilities.sleep(1);

		lp.buildingadd("Apart");
		logger.info("Step-47---> Enter building adress");
		GenericUtilities.sleep(1);

		lp.additionaldetials("Hoodi");
		logger.info("Step-48--> Add addtional address detials");
		GenericUtilities.sleep(2);

		lp.zipcode();
		logger.info("Step--49---> Enter the zip code");
		GenericUtilities.sleep(2);

		lp.Addbutton();
		logger.info("Step-50---> Click on Add button");
		GenericUtilities.sleep(2);

		//lp.Empdate(TestBase.DateMethod());
		logger.info("Step-51---> Enter the date");
		GenericUtilities.sleep(2);

		//lp.Natureofmepl();
		logger.info("Step-52---> Select nature of employment dropdown");
		GenericUtilities.sleep(2);

		//lp.salarycheckbox();
		logger.info("Step-53---> Click on check box");
		GenericUtilities.sleep(2);

		//lp.incomesalarytxt("12345");
		logger.info("Step-54---> enter the Income Salary");
		GenericUtilities.sleep(2);

		lp.otherincometxt("123");
		logger.info("Step-55---> Enter the other income");
		GenericUtilities.sleep(2);

		lp.contbtn3();
		logger.info("step-56--> Click on continue");
		GenericUtilities.sleep(2);
	}
}