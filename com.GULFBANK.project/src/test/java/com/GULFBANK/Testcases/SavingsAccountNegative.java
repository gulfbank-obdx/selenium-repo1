package com.GULFBANK.Testcases;

import java.io.FileInputStream;
import java.io.IOException;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.GULFBANK.Base.TestBase;
import com.GULFBANK.PageObjects.Loginpage;
import com.GULFBANK.Utilities.GenericUtilities;

public class SavingsAccountNegative extends TestBase {

	@Test(dataProvider = "testdata", groups = {"regression"})
	public void savings_accountNegative(String InvalidEmail, String InvalidMobile) throws Throwable {

		Loginpage lp = new Loginpage(driver);
		lp.savingaccount("Click on Saving Account", "Savings account clicked successfully");
		lp.continueguest("Click on Continue Guest button", "Continue Guest button clicked successfully");
		lp.selectCheckBoxes("Select warning checkboxes", "Warning checkbox selected successfully");
		lp.continuebtn("Click on Continue button", "Continue button clicked successfully");
		lp.emailinputtxt(InvalidEmail, "Enter the text in the email field", "Email entered successfully");
		lp.mobileinputtxt(InvalidMobile, "Enter the mobile number", "Mobile number entered successfully");
		lp.continubt1("Click on Continue button", "Continue button clicked successfully");
		if (lp.loginErrorCheck()) {
			GenericUtilities.printExtentPassLog("Errors displayed", "Errors are displayed");
		} else {
			GenericUtilities.printExtentConditionLog(lp.loginErrorCheck(), "Login Errors are not displayed on the UI",
					"Login Errors should be displayed on the UI");
		}
		logger.info("*************End of the Test********");
	}

	@DataProvider(name = "testdata")
	public static Object[][] ReadVariant() throws IOException {
		FileInputStream fileInputStream = new FileInputStream(
				System.getProperty("user.dir") + "/src/test/java/com/GULFBANK/TestData/InputData.xls");
		HSSFWorkbook workbook = new HSSFWorkbook(fileInputStream);
		HSSFSheet worksheet = workbook.getSheet("Sheet2");
		HSSFRow Row = worksheet.getRow(0);

		int RowNum = worksheet.getPhysicalNumberOfRows();
		int ColNum = Row.getLastCellNum();

		Object Data[][] = new Object[RowNum - 1][ColNum];

		for (int i = 0; i < RowNum - 1; i++) {
			HSSFRow row = worksheet.getRow(i + 1);

			for (int j = 0; j < ColNum; j++) {
				if (row == null)
					Data[i][j] = "";
				else {
					HSSFCell cell = row.getCell(j);
					if (cell == null)
						Data[i][j] = "";
					else {
						DataFormatter formatter = new DataFormatter();
						String value = formatter.formatCellValue(cell);
						Data[i][j] = value;
					}
				}
			}
		}
		workbook.close();
		fileInputStream.close();
		return Data;
	}
}