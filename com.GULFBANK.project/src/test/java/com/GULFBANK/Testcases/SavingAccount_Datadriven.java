package com.GULFBANK.Testcases;

import java.awt.AWTException;
import java.io.File;
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.GULFBANK.Base.TestBase;
import com.GULFBANK.PageObjects.Loginpage;
import com.GULFBANK.Utilities.GenericUtilities;
import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class SavingAccount_Datadriven extends TestBase {

	@Test(dataProvider = "testdata")
	public void savingaccount(String emailid, String mobilenumber) throws AWTException

	{

		Loginpage lp = new Loginpage(driver);

		//lp.savingaccount();
		logger.info("Step-1------------->:click on Saving Account");
		GenericUtilities.sleep(2);

		JavascriptExecutor js = (JavascriptExecutor) driver;
		WebElement Element = driver.findElement(By.xpath("(//span[@class='oj-button-text'])[4]"));
		js.executeScript("arguments[0].scrollIntoView();", Element);
		logger.info("Step --> Scroll down to continue guest");
		GenericUtilities.sleep(2);

		//lp.continueguest();
		logger.info("Step-2-----> Click on Continue Guest button");
		GenericUtilities.sleep(2);

		//lp.continuebtn();
		logger.info("Step-3----> Click on Continue button");
		GenericUtilities.sleep(2);

		driver.findElement(
				By.xpath("(//input[@class='oj-inputtext-input oj-text-field-input oj-component-initnode'])[1]"))
				.click();
		driver.findElement(
				By.xpath("(//input[@class='oj-inputtext-input oj-text-field-input oj-component-initnode'])[1]"))
				.sendKeys(emailid);

		// lp.emailinputtxt(emailid);
		logger.info("Step-4 ---> Enter the text in the email field");
		GenericUtilities.sleep(2);

		//lp.mobileinputtxt(mobilenumber);
		logger.info("Step-5-----> Enter the mobile number");
		GenericUtilities.sleep(2);

		//lp.continubt1();
		logger.info("Step -6-----> click on continue");
		GenericUtilities.sleep(4);

		//lp.Otpsentemail("1111");
		logger.info("Step-7---> Enter the Otp number");
		GenericUtilities.sleep(2);

		//lp.verifylnk();
		logger.info("Step -8----> Click on Verify");
		GenericUtilities.sleep(2);

		//lp.Otpsentmobile("1111");
		logger.info("Step-9----> Enter mobile on otp");
		GenericUtilities.sleep(2);

		//lp.verifyotp();
		logger.info("Step-10 ---> Verify otp");
		GenericUtilities.sleep(5);

		//lp.continueotp();
		logger.info("Step-12---> Click on continue");
		GenericUtilities.sleep(5);

		//lp.mobnoverifiedcontinue();
		GenericUtilities.sleep(2);
	}

	@DataProvider(name = "testdata")
	public Object[][] readexcel() throws BiffException, IOException {

		File file = new File(System.getProperty("user.dir") + "/src/test/java/com/GULFBANK/TestData/InputTestData.xls");

		Workbook w = Workbook.getWorkbook(file);
		Sheet s = w.getSheet("SavingsAccount_CivilId");

		int row = s.getRows();
		int colcount = s.getColumns();

		String inputData[][] = new String[row][colcount];

		for (int i = 1; i < row; i++) {
			for (int j = 0; j < colcount; j++) {
				Cell c = s.getCell(j, i);
				inputData[i][j] = c.getContents();
				System.err.println(inputData[i][j]);
			}
		}
		return inputData;
	}
}