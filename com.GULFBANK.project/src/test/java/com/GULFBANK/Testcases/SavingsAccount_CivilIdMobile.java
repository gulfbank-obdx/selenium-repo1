package com.GULFBANK.Testcases;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.FileInputStream;
import java.io.IOException;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.GULFBANK.Base.TestBase;
import com.GULFBANK.PageObjects.Loginpage;
import com.GULFBANK.Utilities.GenericUtilities;

public class SavingsAccount_CivilIdMobile extends TestBase {

	@Test(dataProvider = "testdata")
	public void e2eSavingsAccount(String emailid, String mobileno, String otpsentemail, String otpsentmobile,
			String ApplicantName, String NameasinPassport, String HouseNo, String Buildingnm, String Street,
			String Locality, String Pincode, String FloorNo, String BuildName, String AdditionalDetails, String Zipcode,
			String Income, String otherincome, String Nameoncard) throws AWTException, IOException {

		Loginpage lp = new Loginpage(driver);

		//lp.savingaccount();
		logger.info("Step-1-----> Click on Saving Account");
		GenericUtilities.sleep(2);

		//lp.continueguest();
		logger.info("Step-2-----> Click on Continue Guest button");
		GenericUtilities.sleep(2);

		//lp.selectCheckBoxes();
		logger.info("Step-3-----> Click on Warning checkboxes");
		GenericUtilities.sleep(2);

		//lp.continuebtn();
		logger.info("Step-4----> Click on Continue button");
		GenericUtilities.sleep(2);

		//lp.emailinputtxt(emailid);
		logger.info("Step-5---> Enter the text in the email field");
		GenericUtilities.sleep(2);

		//lp.mobileinputtxt(mobileno);
		logger.info("Step-6-----> Enter the mobile number");
		GenericUtilities.sleep(2);

		//lp.continubt1();
		logger.info("Step-7-----> Click on continue");
		GenericUtilities.sleep(4);

		//lp.Otpsentemail(otpsentemail);
		logger.info("Step-8---> Enter the Otp number");
		GenericUtilities.sleep(2);

		//lp.verifylnk();
		logger.info("Step-9----> Click on Verify");
		GenericUtilities.sleep(2);

		//lp.Otpsentmobile(otpsentmobile);
		logger.info("Step-10----> Enter mobile on otp");
		GenericUtilities.sleep(2);

		//lp.verifyotp();
		logger.info("Step-11---> Verify otp");
		GenericUtilities.sleep(5);

		//lp.continueotp();
		logger.info("Step-12---> Click on continue");
		GenericUtilities.sleep(5);

		lp.Startnewapplication();
		logger.info("Step-13---> Click on start new application");
		GenericUtilities.sleep(5);

		//lp.idtypedropdown();
		logger.info("Step-14----> Click Id type dropdown");
		GenericUtilities.sleep(2);

		//lp.selectIdTypeValue("3");
		logger.info("Step-15----> Click Id type selected");
		GenericUtilities.sleep(2);

		//lp.enterCivilId("120912122626");
		logger.info("Step-16----> Civil Id is entered");
		GenericUtilities.sleep(2);

		//lp.continueAfterIdType();
		logger.info("Step-17----> Click on continue");
		GenericUtilities.sleep(5);

		//lp.titledropdown();
		logger.info("Step-18---> Click on Title dropdown");
		GenericUtilities.sleep(5);
		
		//lp.maritalstatusdrpdown();
		logger.info("Step-19----> Select the Marital Status drop down");
		GenericUtilities.sleep(5);

		//lp.nameasinpassport(NameasinPassport);
		logger.info("Step-20---> Enter the name as in Passport");
		GenericUtilities.sleep(3);

		lp.childrendrpdown();
		logger.info("Step-21----->Select the children drop down");
		GenericUtilities.sleep(3);

		lp.firstdegreerelativedrpdown();
		logger.info("Step-22----> Select the first degree dropdown");
		GenericUtilities.sleep(3);

		lp.flagdrpdown();
		GenericUtilities.sleep(3);
		logger.info("Step-23---> Select the flag dropdown");

		//lp.addaddressbutton();
		logger.info("Step-24---> Click on Add address button");
		GenericUtilities.sleep(2);

		//lp.housenotxt(HouseNo);
		logger.info("Step-25---> Enter House Number");
		GenericUtilities.sleep(3);

		//lp.buildingnametxt(Buildingnm);
		logger.info("Step-26----> Enter Building name");
		GenericUtilities.sleep(2);

		//lp.streettxt(Street);
		logger.info("Step-27----> Enter Street text");
		GenericUtilities.sleep(2);

		//lp.localitytxt(Locality);
		logger.info("Step-28----> Enter locality");
		GenericUtilities.sleep(2);

		//lp.zipcodetxt(Pincode);
		logger.info("Step-29---> Enter zip code");
		GenericUtilities.sleep(2);

		//lp.addbtn();
		logger.info("Step-30----> Click on Add button");
		GenericUtilities.sleep(3);

		//lp.fatcaStatusRadioButton();
		logger.info("Step-31----> Click on FATCA status button");
		GenericUtilities.sleep(3);

		//lp.USCitizenRadioButton();
		logger.info("Step-32----> Click on US Citizen status button");
		GenericUtilities.sleep(3);

		//lp.USResidencyRadioButton();
		logger.info("Step-33----> Click on US Residency status button");
		GenericUtilities.sleep(3);

		//lp.USTaxResidencyRadioButton();
		logger.info("Step-34----> Click on US Tax Residency status button");
		GenericUtilities.sleep(3);

		//lp.contbtn();
		logger.info("Step-35----> Click on Continue button");
		GenericUtilities.sleep(8);

		//lp.skipthisstep();
		logger.info("Step-36----> Click on Skip this step ");
		GenericUtilities.sleep(5);

		//lp.Emptyedrpdown();
		logger.info("Step-37---> Select the Employment drop down");
		GenericUtilities.sleep(2);

		//lp.academicqualifcationdrpdown();
		logger.info("Step-38---> Select the Academic qualifcation");
		GenericUtilities.sleep(2);

		//lp.professiondrpdown();
		logger.info("Step-39---> Select the Profession");
		GenericUtilities.sleep(2);

		//lp.empnamedrpdown();
		logger.info("Step-40---> Select the employer name dropdown");
		GenericUtilities.sleep(2);

		//lp.Empdate(GenericUtilities.DateMethod());
		logger.info("Step-41---> Enter the date");
		GenericUtilities.sleep(2);

		//lp.Natureofmepl();
		logger.info("Step-42---> Select nature of employment dropdown");
		GenericUtilities.sleep(2);

		//lp.salarycheckbox();
		logger.info("Step-43---> Click on salary check box");
		GenericUtilities.sleep(2);

		//lp.incomesalarytxt(Income);
		logger.info("Step-44---> Enter the Income Salary");
		GenericUtilities.sleep(2);

		lp.contbtn3();
		logger.info("Step-45--> Click on continue");
		GenericUtilities.sleep(2);

		lp.applyRegularSavingsAccount();
		logger.info("Step-46--> Click on Regular Savings Account");
		GenericUtilities.sleep(2);

		lp.accountStatementCheckbox();
		logger.info("Step-50---> Click on Account Statement Checkbox");
		GenericUtilities.sleep(2);
		
		lp.debitcarddelivery();
		logger.info("Step-51---> Click on Debit Card delivery");
		GenericUtilities.sleep(2);

		lp.branchLocation();
		logger.info("Step-52---> Click on Branch Location");
		GenericUtilities.sleep(2);
		
		lp.purposeofopening();
		logger.info("Step-53---> Click on Purpose of account opening");
		GenericUtilities.sleep(2);
		
		lp.accountWithAnotherBnk();
		logger.info("Step-54---> Click on Account with another bank");
		GenericUtilities.sleep(2);
		
		lp.natureOfExptdtrn();
		logger.info("Step-55---> Click on Nature of expected transaction");
		GenericUtilities.sleep(2);
		
		lp.cntbtn();
		logger.info("Step-56---> Click on continue");
		GenericUtilities.sleep(4);
		
		
		JavascriptExecutor js = (JavascriptExecutor) driver;
		WebElement Element1 = driver
				.findElement(By.xpath("//span[@data-bind='text:$component.nls.PremierAccount.Continue']"));
		js.executeScript("arguments[0].scrollIntoView();", Element1);
		logger.info("Step --> Scroll down to continue guest");
		GenericUtilities.sleep(5);

		try {
			WebDriverWait wait2 = new WebDriverWait(driver, 60);
			WebElement reason2 = wait2.until(ExpectedConditions.visibilityOfElementLocated(
					By.xpath("//span[@data-bind='text:$component.nls.PremierAccount.Continue']")));
			GenericUtilities.sleep(2);
			reason2.click();
		} catch (StaleElementReferenceException e) {
			WebDriverWait wait2 = new WebDriverWait(driver, 60);
			WebElement reason2 = wait2.until(ExpectedConditions.visibilityOfElementLocated(
					By.xpath("//span[@data-bind='text:$component.nls.PremierAccount.Continue']")));
			GenericUtilities.sleep(2);
			reason2.click();
		}
		
		//lp.uploadbtn();
		logger.info("Step-58---> Click on upload signature buttonn");
		GenericUtilities.sleep(2);

		//lp.uploadbtn1();
		logger.info("Step-59---> Click on upload btn");
		GenericUtilities.sleep(2);

		Robot robot1 = new Robot();
		robot1.setAutoDelay(2000);

		StringSelection stringselection1 = new StringSelection(
				System.getProperty("user.dir") + "\\src\\test\\java\\com\\GULFBANK\\TestData\\signature.jpg");

		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringselection1, null);

		robot1.setAutoDelay(2000);

		robot1.keyPress(KeyEvent.VK_CONTROL);
		robot1.keyPress(KeyEvent.VK_V);

		robot1.keyRelease(KeyEvent.VK_CONTROL);
		robot1.keyRelease(KeyEvent.VK_V);

		robot1.setAutoDelay(2000);
		robot1.keyPress(KeyEvent.VK_ENTER);
		robot1.keyRelease(KeyEvent.VK_ENTER);

		GenericUtilities.sleep(2);
		logger.info("Step-60-----> Impage uploaded sucessfully");

		//lp.agreeradionbtn();
		logger.info("Step-61---> Click on Agreement");
		GenericUtilities.sleep(2);
		
		//lp.confirmbtn();
		logger.info("Step-62---> Click on Confirm button");
		GenericUtilities.sleep(2);
		
		captureScreen(driver, getClass().getName());
		String appNum = lp.getApplicationNumber();
		logger.info("Application number is :" + appNum);
		
		captureScreen(driver, getClass().getName());
		//lp.trackApplication();
		logger.info("Step-63---> Click on Track Application Number");
		GenericUtilities.sleep(2);

		logger.info("*************End of the Test********");
	}

	@DataProvider(name = "testdata")
    public static Object[][] readExcel() throws IOException
    {
        FileInputStream fileInputStream= new FileInputStream(System.getProperty("user.dir") + "/src/test/java/com/GULFBANK/TestData/InputData.xls");
		HSSFWorkbook workbook = new HSSFWorkbook (fileInputStream); 
        HSSFSheet worksheet = workbook.getSheet("Sheet1");
        HSSFRow Row=worksheet.getRow(0); 
     
        int RowNum = worksheet.getPhysicalNumberOfRows();
        int ColNum= Row.getLastCellNum(); 
         
        Object Data[][]= new Object[RowNum-1][ColNum];
         
            for(int i=0; i<RowNum-1; i++) 
            {  
                HSSFRow row= worksheet.getRow(i+1);
                 
                for (int j=0; j<ColNum; j++)
                {
                    if(row==null)
                        Data[i][j]= "";
                    else
                    {
                        HSSFCell cell= row.getCell(j);
                        if(cell==null)
                            Data[i][j]= "";
                        else
                        {
                        	DataFormatter formatter = new DataFormatter();
							String value=formatter.formatCellValue(cell);
                            Data[i][j]=value;
                        }
                    }
                }
            }
        workbook.close();
        fileInputStream.close();
        return Data;
    }
}