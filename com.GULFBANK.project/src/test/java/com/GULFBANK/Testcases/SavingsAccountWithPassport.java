package com.GULFBANK.Testcases;

import java.io.File;
import org.testng.annotations.Test;
import com.GULFBANK.Base.TestBase;
import com.GULFBANK.PageObjects.Loginpage;
import com.GULFBANK.Utilities.GenericUtilities;
import io.restassured.RestAssured;
import io.restassured.response.Response;

public class SavingsAccountWithPassport extends TestBase {

	@Test(groups = {"functional"}, priority = 1)
	public void createSavingsAccountWithPassport() throws Throwable {

		Loginpage lp = new Loginpage(driver);
		lp.savingaccount("Click on Saving Account", "Savings account clicked successfully");
		lp.continueguest("Click on Continue Guest button", "Continue Guest button clicked successfully");
		lp.selectCheckBoxes("Select warning checkboxes", "Warning checkbox selected successfully");
		lp.continuebtn("Click on Continue button", "Continue button clicked successfully");
		lp.emailinputtxt(randomestring() + "@gmail.com", "Enter the text in the email field",
				"Email entered successfully");
		lp.mobileinputtxt("1" + randomeNum(), "Enter the mobile number", "Mobile number entered successfully");
		lp.continubt1("Click on Continue button", "Continue button clicked successfully");
		lp.Otpsentemail("1111", "Enter the OTP number", "OTP entered successfully");
		lp.verifylnk("Click on Verify", "Verify clicked successfully");
		lp.Otpsentmobile("1111", "Enter mobile OTP", "Mobile OTP entered successfully");
		lp.verifyotp("Verify mobile OTP", "Otp verified successfully");
		lp.continueotp("Click on Continue button", "Continue button clicked successfully");
		lp.mobnoverifiedcontinue("Click on Continue button", "Continue button clicked successfully");
		lp.idtypedropdown("Click Id type dropdown", "Id type dropdown selected successfully");
		lp.selectIdTypeValue("1", "Select passport Id type", "Passport Id type selected successfully");
		lp.passportCountry("India", "Enter passport country", "Passport country entered successfully");
		lp.selectordropfilehere("Select the drop file here", "Drop file here selected successfully");
		GenericUtilities.uploadFile("\\src\\test\\java\\com\\GULFBANK\\TestData\\passport_new.PNG");
		lp.continueAfterIdType("Click on Continue button", "Continue button clicked successfully");

		Response response = RestAssured.given().contentType("multipart/form-data")
				.multiPart("file",
						new File(System.getProperty("user.dir")
								+ "\\src\\test\\java\\com\\GULFBANK\\TestData\\passport_new.PNG"))
				.multiPart("country_code", "IND").multiPart("file_name", "passport_new.PNG")
				.multiPart("document_code", "MRZ").multiPart("applicationId", "000APP000000510")
				.multiPart("phone", "83794843").multiPart("email", "uyiuf@gmail.com").multiPart("civilID", "undefined")
				.post("http://10.20.11.7:8888/digx/cz/v1/civildata?locale=en").then().extract().response();

		int statusCode = response.getStatusCode();
		String responsebody = response.asString();
		logger.info("Response code is " + statusCode + "and  response body is " + responsebody);
		logger.info("*************End of the Test********");
	}
}