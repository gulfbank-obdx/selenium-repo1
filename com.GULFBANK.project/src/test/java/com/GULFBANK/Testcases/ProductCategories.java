package com.GULFBANK.Testcases;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.GULFBANK.Base.TestBase;
import com.GULFBANK.PageObjects.ProductCategoriesPage;
import com.GULFBANK.Utilities.GenericUtilities;

public class ProductCategories extends TestBase {

	@Test(groups = { "regression" }, priority = 1)
	public void verificationOfPageTitle() throws Throwable {
		ProductCategoriesPage productCategories = new ProductCategoriesPage(driver);
		WebElement title = productCategories.getTitle();
		Boolean titleFlag;
		String text = null;
		String expected = "Our Product Offerings";
		if (title.isDisplayed()) {
			text = title.getText();
			titleFlag = text.equals(expected);
			if (titleFlag) {
				GenericUtilities.printExtentConditionLog(titleFlag, "Page title should return successfully",
						"Page title returned");
			} else {
				GenericUtilities.printExtentConditionLog(titleFlag, "Page title should return successfully",
						"Page title returned");
				Assert.assertTrue(text.equals(expected), "Page title not returned");
			}
		}
	}

	@Test(dependsOnMethods = { "verificationOfPageTitle" }, groups = { "regression", "functional" }, priority = 5)
	public void verificationOfProductsCategories() throws Throwable {
		ProductCategoriesPage productCategories = new ProductCategoriesPage(driver);
		List<String> actualList = new ArrayList<>();
		List<String> expectedList = Arrays.asList("Savings Accounts", "Checking Accounts", "Credit Cards", "Auto Loans",
				"Personal Loans");
		actualList = productCategories.getCategoryList();
		Boolean compareFlag = expectedList.equals(actualList);
		if (compareFlag) {
			GenericUtilities.printExtentConditionLog(compareFlag, "Category list should be present",
					"Category list present");
		} else {
			GenericUtilities.printExtentConditionLog(compareFlag, "Category list should be present",
					"Category list present");
			Assert.assertTrue(compareFlag, "Category list not present as expected");
		}
	}

	@Test(groups = { "regression", "functional" }, priority = 2)
	public void verificationOfProductsImages() throws Throwable {
		ProductCategoriesPage productCategories = new ProductCategoriesPage(driver);
		for (WebElement image : productCategories.getImageList()) {
			if (image.isDisplayed()) {
				GenericUtilities.HighlightElement(driver, image);
				GenericUtilities.printExtentPassLog("Product image should be present", "Product image is present");
			} else {
				GenericUtilities.printExtentConditionLog(image.isDisplayed(), "Product image should be present",
						"Product image is present");
				Assert.assertTrue(image.isDisplayed(), "Product icons are not present as expected");
			}
		}
	}

	@Test(groups = { "regression", "functional" }, priority = 3)
	public void verificationOfKnowMore() throws Throwable {
		ProductCategoriesPage productCategories = new ProductCategoriesPage(driver);
		for (int i = 1; i <= productCategories.getKnowMoreList().size(); i++) {
			String xpath = "[" + Integer.toString(i) + "]";
			WebElement knowmore = productCategories.getKnowMore(xpath);
			if (knowmore.isDisplayed()) {
				GenericUtilities.HighlightElement(driver, knowmore);
				knowmore.click();
				productCategories.getHomeHyperlink();
				driver.navigate().back();
				GenericUtilities.printExtentPassLog("Know more link should be present", "Know more link is present");
			} else {
				GenericUtilities.printExtentConditionLog(knowmore.isDisplayed(), "Know more link should be present",
						"Know more link is present");
				Assert.assertTrue(knowmore.isDisplayed(), "Know more is not present as expected");
			}
		}
	}

	@Test(groups = { "regression", "functional" }, priority = 4)
	public void verificationOfKnowMoreSavingsAccount() throws Throwable {
		ProductCategoriesPage productCategories = new ProductCategoriesPage(driver);
		WebElement knowmore = productCategories.getKnowMoreSavingsAccount();
		Boolean flag = knowmore.isDisplayed();
		if (flag) {
			GenericUtilities.HighlightElement(driver, productCategories.getKnowMoreSavingsAccount());
			productCategories.getKnowMoreSavingsAccount().click();
			productCategories.getHomeHyperlink();
			driver.navigate().back();
			driver.navigate().refresh();
			WebElement knowmore1 = productCategories.getKnowMoreSavingsAccount();
			GenericUtilities.elementVisible(knowmore1);
			;
			GenericUtilities.printExtentPassLog("Know more link should be present", "Know more link is present");
		} else {
			GenericUtilities.printExtentConditionLog(flag, "Know more link should be present",
					"Know more link is present");
			Assert.assertTrue(flag, "Know more is not present as expected");
		}
	}
}