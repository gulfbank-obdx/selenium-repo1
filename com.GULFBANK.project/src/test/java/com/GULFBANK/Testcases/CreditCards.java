package com.GULFBANK.Testcases;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import com.GULFBANK.Base.TestBase;
import com.GULFBANK.PageObjects.Creditcards;
import com.GULFBANK.PageObjects.Loginpage;
import com.GULFBANK.Utilities.GenericUtilities;

public class CreditCards extends TestBase {

	@Test
	public void e2eCreditCards() throws AWTException, IOException {

		Loginpage lp = new Loginpage(driver);
		Creditcards cards = new Creditcards(driver);

		cards.creditcard();
		logger.info("Step-1-----> Click on credit card");
		GenericUtilities.sleep(2);

		//lp.continueguest();
		logger.info("Step-2-----> Click on Continue Guest button");
		GenericUtilities.sleep(2);

		//lp.selectCheckBoxes();
		logger.info("Step-3-----> Click on Checkboxes");
		GenericUtilities.sleep(2);

		//lp.continuebtn();
		logger.info("Step-4----> Click on Continue button");
		GenericUtilities.sleep(2);

		//lp.emailinputtxt(randomestring() + "@gmail.com");
		logger.info("Step-5 ----> Enter the text in the email field");
		GenericUtilities.sleep(2);

		//lp.mobileinputtxt("1" + randomeNum());
		logger.info("Step-6 ----> Enter the mobile number");
		GenericUtilities.sleep(2);

		//lp.continubt1();
		logger.info("Step-7 ----> Click on continue");
		GenericUtilities.sleep(4);

		//lp.Otpsentemail("1111");
		logger.info("Step-8 ---> Enter the Otp number");
		GenericUtilities.sleep(2);

		//lp.verifylnk();
		logger.info("Step-9 ----> Click on Verify");
		GenericUtilities.sleep(2);

		//lp.Otpsentmobile("1111");
		logger.info("Step-10 ----> Enter mobile on otp");
		GenericUtilities.sleep(2);

		//lp.verifyotp();
		logger.info("Step-11 ---> Verify otp");
		GenericUtilities.sleep(5);

		//lp.continueotp();
		logger.info("Step-12---> Click on continue");
		GenericUtilities.sleep(5);

		//lp.mobnoverifiedcontinue();
		GenericUtilities.sleep(2);

		// lp.Startnewapplication();
		logger.info("Step-13----> Click on continue on mobile verified page");
		GenericUtilities.sleep(2);

		//lp.idtypedropdown();
		logger.info("Step-14 ----> Click Id type dropdown");
		GenericUtilities.sleep(2);

		//lp.selectIdTypeValue("3");
		logger.info("Step-15 ----> Click Id type selected");
		GenericUtilities.sleep(2);

		//lp.enterCivilId("120912122626");
		logger.info("Step-16 ----> Enter Civil Id");
		GenericUtilities.sleep(2);

		//lp.continueAfterIdType();
		logger.info("Step-17 ----> Click on continue");
		GenericUtilities.sleep(5);

		/*
		 * List<WebElement> opt1 =
		 * driver.findElements(By.xpath("(//span[@class='oj-select-chosen'])[2]")); int
		 * s1 = opt1.size(); // Iterating through the list selecting the desired option
		 * for (int j = 0; j < opt1.size(); j++) { // if the option is By Subject click
		 * that option if (opt1.get(j).getText().equals("Back")) { opt1.get(j).click();
		 * break; } }
		 * 
		 * // lp.documentsidedrpdwn();
		 * logger.info("Step-14----> select the document side dropdown");
		 * GenericUtilities.sleep(8);
		 * 
		 * lp.selectordropfilehere();
		 * logger.info("Step-15-----> Select the drop file here");
		 * GenericUtilities.sleep(8);
		 * 
		 * Robot robot = new Robot(); robot.setAutoDelay(2000);
		 * 
		 * StringSelection stringselection = new StringSelection(
		 * System.getProperty("user.dir") +
		 * "\\src\\test\\java\\com\\GULFBANK\\TestData\\Himanshu_Civil ID.jpg");
		 * 
		 * Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringselection,
		 * null);
		 * 
		 * robot.setAutoDelay(2000);
		 * 
		 * robot.keyPress(KeyEvent.VK_CONTROL); robot.keyPress(KeyEvent.VK_V);
		 * 
		 * robot.keyRelease(KeyEvent.VK_CONTROL); robot.keyRelease(KeyEvent.VK_V);
		 * 
		 * robot.setAutoDelay(2000); robot.keyPress(KeyEvent.VK_ENTER);
		 * robot.keyRelease(KeyEvent.VK_ENTER);
		 * 
		 * GenericUtilities.sleep(2);
		 * logger.info(" Step-16----> Impage uploaded sucessfully");
		 * 
		 * lp.continue2(); logger.info("Step-17----> Click on continue");
		 * GenericUtilities.sleep(5);
		 */

		/*
		 * GenericUtilities.sleep(5); lp.applicanttxt(randomestring());
		 * logger.info("Step -19----> Enter the name of the applicant");
		 * GenericUtilities.sleep(5);
		 */

		//lp.titledropdown();
		logger.info("Step-18 ----> Click on Title dropdown");
		GenericUtilities.sleep(5);

		//lp.maritalstatusdrpdown();
		logger.info("Step-19 ----> Select the Marital Status drop down");
		GenericUtilities.sleep(5);

		/*
		 * lp.Citizenshipdrpdown();
		 * logger.info("Step-21---> Select the citizenship dropdown");
		 * GenericUtilities.sleep(5);
		 */

		//lp.nameasinpassport("Himanshu");
		logger.info("Step-20 ----> Enter the name as in Passport");
		GenericUtilities.sleep(3);

		/*
		 * lp.othernationalitesdrpdown();
		 * logger.info("Step-23---> Select the other Nationalities dropdonw");
		 * GenericUtilities.sleep(3);
		 * 
		 * lp.countryofbirthdropdown();
		 * logger.info("Step-23----> Selec the country of birth drop down");
		 * GenericUtilities.sleep(3);
		 * 
		 * lp.childrendrpdown();
		 * logger.info("Step-24----->Select the children drop down");
		 * GenericUtilities.sleep(3);
		 * 
		 * lp.firstdegreerelativedrpdown();
		 * logger.info("Step-25----> Select the first degree dropdown");
		 * GenericUtilities.sleep(3);
		 * 
		 * lp.flagdrpdown(); GenericUtilities.sleep(3);
		 * logger.info("Step-26---> Select the flag dropdown");
		 * 
		 * lp.contactdetials(); logger.info("Step-27---->Click on contact detials");
		 * GenericUtilities.sleep(3);
		 */

		//lp.addaddressbutton();
		logger.info("Step-21 ----> Click on Add address button");
		GenericUtilities.sleep(2);

		/*
		 * lp.addaddrestxt("Bangalore"); logger.info("Step-30----> Enter the Address");
		 * GenericUtilities.sleep(2);
		 * 
		 * lp.searchicon(); logger.info("Step-31---> Click on Search icon");
		 * GenericUtilities.sleep(2);
		 */

		//lp.housenotxt("316");
		logger.info("Step-22 ----> Enter House Number");
		GenericUtilities.sleep(3);

		//lp.buildingnametxt("Apartment");
		logger.info("Step-23 ----> Enter Building name");
		GenericUtilities.sleep(2);

		//lp.streettxt("Hoodi");
		logger.info("Step-24 ----> Enter Street text");
		GenericUtilities.sleep(2);

		//lp.localitytxt("Hoodi Circle");
		logger.info("Step-25 ----> Enter locality");
		GenericUtilities.sleep(2);

		//lp.zipcodetxt("560048");
		logger.info("Step-26 ----> Enter zip code");
		GenericUtilities.sleep(2);

		//lp.addbtn();
		logger.info("Step-27----> Click on Add button");
		GenericUtilities.sleep(3);

		//lp.fatcaStatusRadioButton();
		logger.info("Step-28 -----> Click on FATCA status button");
		GenericUtilities.sleep(3);

		//lp.USCitizenRadioButton();
		logger.info("Step-29 ----> Click on US Citizen status button");
		GenericUtilities.sleep(3);

		//lp.USResidencyRadioButton();
		logger.info("Step-30 ----> Click on US Residency status button");
		GenericUtilities.sleep(3);

		//lp.USTaxResidencyRadioButton();
		logger.info("Step-31 ----> Click on US Tax Residency status button");
		GenericUtilities.sleep(3);

		//lp.contbtn();
		logger.info("Step-32 ----> Click on Continue button");
		GenericUtilities.sleep(8);

		//lp.skipthisstep();
		logger.info("Step-33 ----> Click on skip this step ");
		GenericUtilities.sleep(5);

		//lp.Emptyedrpdown();
		logger.info("Step-34 ----> Select the Employment drop down");
		GenericUtilities.sleep(2);

		//lp.academicqualifcationdrpdown();
		logger.info("Step-35 ----> Select the Academic qualifcation");
		GenericUtilities.sleep(2);

		//lp.professiondrpdown();
		logger.info("Step-36 ----> Select the Profession");
		GenericUtilities.sleep(2);

		//lp.empnamedrpdown();
		logger.info("Step-37 ----> Select the Employer name dropdown");
		GenericUtilities.sleep(2);
		/*
		 * lp.addworkaddress(); logger.info("Step-45----> Click on Work address");
		 * GenericUtilities.sleep(2);
		 * 
		 * lp.floorbayddress("111"); logger.info("Step-46---> Enter floor bay No");
		 * GenericUtilities.sleep(1);
		 * 
		 * lp.buildingadd("Apart"); logger.info("Step-47---> Enter building adress");
		 * GenericUtilities.sleep(1);
		 * 
		 * lp.additionaldetials("Hoodi");
		 * logger.info("Step-48--> Add addtional address detials");
		 * GenericUtilities.sleep(2);
		 * 
		 * lp.zipcode(); logger.info("Step--49---> Enter the zip code");
		 * GenericUtilities.sleep(2);
		 * 
		 * lp.Addbutton(); logger.info("Step-50---> Click on Add button");
		 * GenericUtilities.sleep(2);
		 */

		//lp.Empdate(TestBase.DateMethod());
		logger.info("Step-38 ----> Enter the date");
		GenericUtilities.sleep(2);

		//lp.Natureofmepl();
		logger.info("Step-39 ----> Select nature of employment dropdown");
		GenericUtilities.sleep(2);

		//lp.salarycheckbox();
		logger.info("Step-40 ----> Click on salary check box");
		GenericUtilities.sleep(2);

		//lp.incomesalarytxt("12345");
		logger.info("Step-41 ----> Enter the Income Salary");
		GenericUtilities.sleep(2);

		lp.contbtn3();
		logger.info("Step-42 ----> Click on continue");
		GenericUtilities.sleep(2);

		/*
		 * lp.otherincometxt("123"); logger.info("Step-55---> Enter the other income");
		 * GenericUtilities.sleep(2);
		 */

		JavascriptExecutor js = (JavascriptExecutor) driver;
		WebElement ele = driver
				.findElement(By.xpath("//span[@data-bind='text:$component.nls.PremierAccount.Continue']"));
		js.executeScript("arguments[0].scrollIntoView();", ele);
		logger.info("Step 43 ---> Scroll down to continue");
		GenericUtilities.sleep(5);

		try {
			WebDriverWait wait2 = new WebDriverWait(driver, 60);
			WebElement reason2 = wait2.until(ExpectedConditions.visibilityOfElementLocated(
					By.xpath("//span[@data-bind='text:$component.nls.PremierAccount.Continue']")));
			GenericUtilities.sleep(2);
			reason2.click();
		} catch (StaleElementReferenceException e) {
			WebDriverWait wait2 = new WebDriverWait(driver, 60);
			WebElement reason2 = wait2.until(ExpectedConditions.visibilityOfElementLocated(
					By.xpath("//span[@data-bind='text:$component.nls.PremierAccount.Continue']")));
			GenericUtilities.sleep(2);
			reason2.click();
		}

		lp.platinumChipCreditCard();
		logger.info("Step-44 ----> Click on Platinum Chip credit card button");
		GenericUtilities.sleep(5);

		WebElement ele1 = driver
				.findElement(By.xpath("//span[@data-bind='text:$component.nls.PremierAccount.Continue']"));
		js.executeScript("arguments[0].scrollIntoView();", ele1);
		logger.info("Step-45 ---> Scroll down to continue");
		GenericUtilities.sleep(5);

		try {
			WebDriverWait wait2 = new WebDriverWait(driver, 60);
			WebElement reason2 = wait2.until(ExpectedConditions.visibilityOfElementLocated(
					By.xpath("//span[@data-bind='text:$component.nls.PremierAccount.Continue']")));
			GenericUtilities.sleep(2);
			reason2.click();
		} catch (StaleElementReferenceException e) {
			WebDriverWait wait2 = new WebDriverWait(driver, 60);
			WebElement reason2 = wait2.until(ExpectedConditions.visibilityOfElementLocated(
					By.xpath("//span[@data-bind='text:$component.nls.PremierAccount.Continue']")));
			GenericUtilities.sleep(2);
			reason2.click();
		}

		WebElement ele2 = driver
				.findElement(By.xpath("//span[@data-bind='text:$component.nls.PremierAccount.Continue']"));
		js.executeScript("arguments[0].scrollIntoView();", ele2);
		logger.info("Step 46 ----> Scroll down to continue");
		GenericUtilities.sleep(5);

		try {
			WebDriverWait wait2 = new WebDriverWait(driver, 60);
			WebElement reason2 = wait2.until(ExpectedConditions.visibilityOfElementLocated(
					By.xpath("//span[@data-bind='text:$component.nls.PremierAccount.Continue']")));
			GenericUtilities.sleep(2);
			reason2.click();
		} catch (StaleElementReferenceException e) {
			WebDriverWait wait2 = new WebDriverWait(driver, 60);
			WebElement reason2 = wait2.until(ExpectedConditions.visibilityOfElementLocated(
					By.xpath("//span[@data-bind='text:$component.nls.PremierAccount.Continue']")));
			GenericUtilities.sleep(2);
			reason2.click();
		}

		//lp.uploadbtn();
		logger.info("Step-47 ---> Click on upload signature buttonn");
		GenericUtilities.sleep(2);

		//lp.uploadbtn1();
		logger.info("Step-48 ---> Click on upload btn");
		GenericUtilities.sleep(2);

		Robot robot1 = new Robot();
		robot1.setAutoDelay(2000);

		StringSelection stringselection2 = new StringSelection(
				System.getProperty("user.dir") + "\\src\\test\\java\\com\\GULFBANK\\TestData\\signature.jpg");

		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringselection2, null);

		robot1.setAutoDelay(2000);

		robot1.keyPress(KeyEvent.VK_CONTROL);
		robot1.keyPress(KeyEvent.VK_V);

		robot1.keyRelease(KeyEvent.VK_CONTROL);
		robot1.keyRelease(KeyEvent.VK_V);

		robot1.setAutoDelay(2000);
		robot1.keyPress(KeyEvent.VK_ENTER);
		robot1.keyRelease(KeyEvent.VK_ENTER);

		GenericUtilities.sleep(2);
		logger.info("Step-49 ----> Image uploaded sucessfully");

		//lp.agreeradionbtn();
		logger.info("Step-50 ----> Click on Agreement");
		GenericUtilities.sleep(2);

		//lp.confirmbtn();
		logger.info("Step-51 ---> Click on Confirm button");
		GenericUtilities.sleep(2);

		String appNum = lp.getApplicationNumber();
		captureScreen(driver, getClass().getName());
		logger.info("Application number is :" + appNum);

		//lp.trackApplication();
		captureScreen(driver, getClass().getName());
		logger.info("Step-52 ----> Click on Track Application Number");
		GenericUtilities.sleep(2);

		logger.info("*************End of the Test********");

		/*
		 * cards.creditcard(); GenericUtilities.sleep(2);
		 * 
		 * cards.cardinternationally(); GenericUtilities.sleep(2);
		 * 
		 * cards.transferlink(); GenericUtilities.sleep(2);
		 * 
		 * cards.Addoncard(); GenericUtilities.sleep(2);
		 */
	}
}