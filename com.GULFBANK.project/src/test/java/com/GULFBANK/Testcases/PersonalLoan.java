package com.GULFBANK.Testcases;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import java.io.FileInputStream;
import java.io.IOException;

import com.GULFBANK.Base.ExtentTestManager;
import com.GULFBANK.Base.TestBase;
import com.GULFBANK.PageObjects.Loginpage;
import com.GULFBANK.PageObjects.PersonalLoanPage;
import com.GULFBANK.Utilities.GenericUtilities;
import com.aventstack.extentreports.Status;

public class PersonalLoan extends TestBase {

	@Test(dataProvider = "testdata", groups = { "regression" })
	public void e2ePersonalLoan(String emailid, String mobileno, String otpsentemail, String otpsentmobile,
			String ApplicantName, String NameasinPassport, String HouseNo, String Buildingnm, String Street,
			String Locality, String Pincode, String FloorNo, String BuildName, String AdditionalDetails, String Zipcode,
			String Income, String otherincome, String Nameoncard) throws Throwable {

		PersonalLoanPage pl = new PersonalLoanPage(driver);
		Loginpage lp = new Loginpage(driver);
		
		pl.PersonalLoan("Click on Personal loan", "Personal loan clicked successfully");
		lp.continueguest("Click on Continue Guest button", "Continue Guest button clicked successfully");
		lp.selectCheckBoxes("Select warning checkboxes", "Warning checkbox selected successfully");
		lp.continuebtn("Click on Continue button", "Continue button clicked successfully");
		lp.emailinputtxt(randomestring() + "@gmail.com", "Enter the text in the email field",
				"Email entered successfully");
		lp.mobileinputtxt("1" + randomeNum(), "Enter the mobile number", "Mobile number entered successfully");
		lp.continubt1("Click on Continue button", "Continue button clicked successfully");
		lp.Otpsentemail("1111", "Enter the OTP number", "OTP entered successfully");
		lp.verifylnk("Click on Verify", "Verify clicked successfully");
		lp.Otpsentmobile("1111", "Enter mobile OTP", "Mobile OTP entered successfully");
		lp.verifyotp("Verify mobile OTP", "Otp verified successfully");
		lp.continueotp("Click on Continue button", "Continue button clicked successfully");
		lp.mobnoverifiedcontinue("Click on Continue button", "Continue button clicked successfully");
		lp.idtypedropdown("Click Id type dropdown", "Id type dropdown selected successfully");
		lp.selectIdTypeValue("3", "Select civil Id with mobile type",
				"Civil Id with mobile type selected successfully");
		lp.enterCivilId("120912122626", "Civild is entered", "Enter Civil Id successfully");
		lp.continueAfterIdType("Click on Continue button", "Continue button clicked successfully");
		lp.titledropdown("Click on Title dropdown", "Title dropdown selected successfully");
		lp.maritalstatusdrpdown("Select the Marital Status drop down", "Marital Status drop down selected");
		lp.nameasinpassport(NameasinPassport, "Enter the name as in Passport", "Passport name entered successfully");
		lp.addaddressbutton("Click on Add address button", "Children dropdown selected successfully");
		lp.housenotxt(HouseNo, "Enter House Number", "House Number entered successfully");
		lp.buildingnametxt(Buildingnm, "Enter Building name", "Building name entered successfully");
		lp.streettxt(Street, "Enter Street text", "Street text entered successfully");
		lp.zipcodetxt(Pincode, "Enter zip code", "Zip code entered successfully ");
		lp.localitytxt(Locality, "Enter locality", "Locality entered successfully");
		lp.addbtn("Click on Add button", "Add button selected");
		lp.fatcaStatusRadioButton("Click on FATCA status button", "FATCA status button selected");
		lp.USCitizenRadioButton("Click on US Citizen status button", "US Citizen status button selected");
		lp.USResidencyRadioButton("Click on US Residency status button", "US Residency status button selected");
		lp.USTaxResidencyRadioButton("Click on US Tax Residency status button",
				"US Tax Residency status button selected");
		lp.contbtn("Click on Continue button", "Continue button clicked successfully");
		lp.skipthisstep("Click on Skip this step", "Skip this step clicked successfully");
		lp.Emptyedrpdown("Select the Employment Type drop down", "Employment Type drop down selected successfully");
		lp.academicqualifcationdrpdown("Select the Academic qualifcation",
				"Academic qualifcation selected successfully");
		lp.professiondrpdown("Select the Profession", "Profession selected successfully");
		lp.empnamedrpdown("Select the Employer name dropdown", "Employer name dropdown selected successfully");
		lp.Empdate(TestBase.DateMethod(), "Enter the date", "Date selected successfully ");
		lp.Natureofmepl("Select nature of employer activity dropdown",
				"Nature of employer activity dropdown selected successfully");
		lp.salarycheckbox("Click on salary check box", "Salary check box selected successfully");
		lp.incomesalarytxt("1234", "Enter the Income Salary", "Income Salary entered successfully");
		lp.continubt1("Click on Continue button", "Continue button clicked successfully");

		JavascriptExecutor js = (JavascriptExecutor) driver;
		WebElement Element1 = driver
				.findElement(By.xpath("//span[@data-bind='text:$component.nls.PremierAccount.Continue']"));
		js.executeScript("arguments[0].scrollIntoView();", Element1);
		logger.info("Step 43 ----> Scroll down to continue");
		GenericUtilities.sleep(5);

		try {
			WebDriverWait wait2 = new WebDriverWait(driver, 60);
			WebElement reason2 = wait2.until(ExpectedConditions.visibilityOfElementLocated(
					By.xpath("//span[@data-bind='text:$component.nls.PremierAccount.Continue']")));
			GenericUtilities.sleep(2);
			reason2.click();
		} catch (StaleElementReferenceException e) {
			WebDriverWait wait2 = new WebDriverWait(driver, 60);
			WebElement reason2 = wait2.until(ExpectedConditions.visibilityOfElementLocated(
					By.xpath("//span[@data-bind='text:$component.nls.PremierAccount.Continue']")));
			GenericUtilities.sleep(2);
			reason2.click();
		}

		lp.applyConsumerLoan("Click on Consumer Loan", "Consumer Loan clicked successfully");
		pl.loanDetailsCollapse("Click on loan details", "Loan details clicked successfully");
		pl.longtermyear("Enter long term", "Long term entered successfully");

		WebElement Element12 = driver
				.findElement(By.xpath("//span[@data-bind='text:$component.nls.PremierAccount.Continue']"));
		js.executeScript("arguments[0].scrollIntoView();", Element12);
		logger.info("Step 47 ----> Scroll down to continue");
		GenericUtilities.sleep(5);

		try {
			WebDriverWait wait3 = new WebDriverWait(driver, 60);
			WebElement reason3 = wait3.until(ExpectedConditions.visibilityOfElementLocated(
					By.xpath("(//span[@data-bind='text:$component.nls.PremierAccount.Continue'])")));
			GenericUtilities.sleep(2);
			reason3.click();

		} catch (StaleElementReferenceException e) {
			WebDriverWait wait3 = new WebDriverWait(driver, 60);
			WebElement reason3 = wait3.until(ExpectedConditions.visibilityOfElementLocated(
					By.xpath("(//span[@data-bind='text:$component.nls.PremierAccount.Continue'])")));
			GenericUtilities.sleep(2);
			reason3.click();
		}

		lp.selectordropfilehere("Select the drop file here", "Drop file here selected successfully");
		GenericUtilities.uploadFile("\\src\\test\\java\\com\\GULFBANK\\TestData\\Himanshu_Civil ID.jpg");
		pl.selectDocType("Select the document type", "Document type selected successfully");

		WebElement ele1 = driver
				.findElement(By.xpath("//span[@data-bind='text:$component.nls.PremierAccount.Continue']"));
		js.executeScript("arguments[0].scrollIntoView();", ele1);
		logger.info("Step-52 ----> Scroll down to continue");
		GenericUtilities.sleep(5);

		try {
			WebDriverWait wait2 = new WebDriverWait(driver, 60);
			WebElement reason2 = wait2.until(ExpectedConditions.visibilityOfElementLocated(
					By.xpath("//span[@data-bind='text:$component.nls.PremierAccount.Continue']")));
			GenericUtilities.sleep(2);
			reason2.click();
		} catch (StaleElementReferenceException e) {
			WebDriverWait wait2 = new WebDriverWait(driver, 60);
			WebElement reason2 = wait2.until(ExpectedConditions.visibilityOfElementLocated(
					By.xpath("//span[@data-bind='text:$component.nls.PremierAccount.Continue']")));
			GenericUtilities.sleep(2);
			reason2.click();
		}

		WebElement ele2 = driver
				.findElement(By.xpath("//span[@data-bind='text:$component.nls.PremierAccount.Continue']"));
		js.executeScript("arguments[0].scrollIntoView();", ele2);
		logger.info("Step-53 ----> Scroll down to continue");
		GenericUtilities.sleep(5);

		try {
			WebDriverWait wait2 = new WebDriverWait(driver, 60);
			WebElement reason2 = wait2.until(ExpectedConditions.visibilityOfElementLocated(
					By.xpath("//span[@data-bind='text:$component.nls.PremierAccount.Continue']")));
			GenericUtilities.sleep(2);
			reason2.click();
		} catch (StaleElementReferenceException e) {
			WebDriverWait wait2 = new WebDriverWait(driver, 60);
			WebElement reason2 = wait2.until(ExpectedConditions.visibilityOfElementLocated(
					By.xpath("//span[@data-bind='text:$component.nls.PremierAccount.Continue']")));
			GenericUtilities.sleep(2);
			reason2.click();
		}

		lp.uploadbtn("Click on upload signature button", "Upload signature button clicked successfully");
		lp.uploadbtn1("Click on upload button", "Upload button clicked successfully");
		GenericUtilities.uploadFile("\\src\\test\\java\\com\\GULFBANK\\TestData\\signature.jpg");
		lp.agreeradionbtn("Click on Agreement", "Agreement clicked successfully");
		lp.confirmbtn("Click on Confirm button", "Confirm button clicked successfully");
		String appNum = lp.getApplicationNumber();
		ExtentTestManager.getTest().log(Status.INFO, "Application number is :" + appNum);
		lp.trackApplication("Click on Track Application Number", "Track Application Number clicked successfully");
		logger.info("Step-59 -----> Click on Track Application Number");
		logger.info("*************End of the Test********");
	}

	@DataProvider(name = "testdata")
	public static Object[][] readExcel() throws IOException {
		FileInputStream fileInputStream = new FileInputStream(
				System.getProperty("user.dir") + "/src/test/java/com/GULFBANK/TestData/InputData.xls");
		HSSFWorkbook workbook = new HSSFWorkbook(fileInputStream);
		HSSFSheet worksheet = workbook.getSheet("Sheet1");
		HSSFRow Row = worksheet.getRow(0);

		int RowNum = worksheet.getPhysicalNumberOfRows();
		int ColNum = Row.getLastCellNum();

		Object Data[][] = new Object[RowNum - 1][ColNum];

		for (int i = 0; i < RowNum - 1; i++) {
			HSSFRow row = worksheet.getRow(i + 1);

			for (int j = 0; j < ColNum; j++) {
				if (row == null)
					Data[i][j] = "";
				else {
					HSSFCell cell = row.getCell(j);
					if (cell == null)
						Data[i][j] = "";
					else {
						DataFormatter formatter = new DataFormatter();
						String value = formatter.formatCellValue(cell);
						Data[i][j] = value;
					}
				}
			}
		}
		workbook.close();
		fileInputStream.close();
		return Data;
	}
}